REQUISITOS

1 - Python 2.7.x - 			 www.python.org
2 - PySide	 - 				 qt-project.org
3 - PyInstaller	 - 			 pythonhosted.org/PyInstaller
4 - python module requests - docs.python-requests.org
5 - PHP Slim framework - 	 docs.slimframework.com/
