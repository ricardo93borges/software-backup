<?php
require 'Slim/Slim.php';
require 'Slim/Middleware.php';
require 'Slim/Extras/Middleware/HttpBasicAuth.php';
require_once 'lib/database.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$app->add(new HttpBasicAuth());

$app->post(	 '/user',        	function()   use($app) { addUser($app);});
$app->post(	 '/auth',        	function()   use($app) { auth($app);});

$app->get(	 '/arquivos',    	function()   use($app) { selectFiles($app);});
$app->get(	 '/arquivo/:id', 	function()   use($app) { selectFile($app, $id);});
$app->post(	 '/arquivo/',    	function()   use($app) { addFile($app);});
$app->put(	 '/arquivo/',    	function()   use($app) { updateFileStatus($app);});
$app->delete('/arquivo/', 		function()   use($app) { deleteFile($app);});

$app->post(	 '/schedule',       function()   use($app) { addSchedule($app);});
$app->get(	 '/schedules',    	function()   use($app) { selectSchedules($app);});
$app->get(	 '/schedule/:id', 	function()   use($app) { selectSchedule($app, $id);});
$app->put(	 '/schedule',    	function()   use($app) { updateSchedule($app);});
$app->delete('/schedule/', 		function()   use($app) { deleteSchedule($app);});

$app->get(	 '/schedulesToBackup/', 		function()   use($app) { selectSchedulesToBackup($app);});
$app->put(	 '/updateScheduleLastBackup/', 	function()   use($app) { updateScheduleLastBackup($app);});


/*
 * HttpBasicAuth 	=> curl -i -X GET  --user user:passwd http://localhost/webservice/index.php/arquivos
 * HttpDigestAuth 	=>
 */

function auth($app){
	try{
		$user = getUser($app);
		if(!$user){
			echo '{"return":"false", "message":"Usuario nao encontrado."}';
		}else{
			echo '{"return":"true",  "message":"Autenticado"}';
		}
	} catch (Exception $e) {
		echo json_encode(array('return'=>'false', 'message'=>$e->getMessage()));
	}
}

function getUser($app){
	$login 		= base64_decode($app->request()->headers('PHP_AUTH_USER'));
	$password 	= base64_decode($app->request()->headers('PHP_AUTH_PW'));
	
	try {
		$c = getConnection();
		$stat = $c->prepare('SELECT * FROM users WHERE login = :login');
		$stat->bindParam(':login', $login);

		$stat->execute();
		$user = $stat->fetchAll();
		$c = null;

		if(count($user) == 1){
			if(password_verify($password, $user[0]['password'])){
				return $user;
			}
		}
		return false;
	} catch (Exception $e) {
		echo json_encode(array('return'=>'false', 'message'=>$e->getMessage()));
	}
}

//curl -X POST -d "login=login" -d "password=password" http://localhost/slim/index.php/user
function addUser($app){
	require_once 'lib/database.php';
	
	$login  	= $app->request()->post('login');
	$password 	= $app->request()->post('password');
	$status		= 1;
	
	try {
		$c = getConnection();
		$stat = $c->prepare('INSERT INTO users VALUES (NULL, :login, :password, :status)');
		$stat->bindParam(':login',    $login);
		$stat->bindParam(':password', $password);
		$stat->bindParam(':status',   $status);
		$stat->execute();
	
		if($stat->rowCount() == 1){
			echo '{"return":"true",  "message":"Novo usuario inserido, id = '.$c->lastInsertId().'"}';
		}else{
			echo '{"return":"false", "message":"falha ao adicionar usuario. Dados enviados: login: '.$login.', password: '.$password.'"}';
		}
	
		$c = null;
	} catch (Exception $e) {
		echo json_encode(array('return'=>'false', 'message'=>$e->getMessage()));
	}
}

function selectFiles($app){
	require_once 'lib/database.php';
	
	$user = getUser($app);
	if(!$user){
		echo '{"return":"false", "message":"Usuario nao encontrado."}';
		die();
	}	
	$user = $user[0]['id'];

	try {
		$c 	  = getConnection();
		$stat = $c->prepare('SELECT * FROM files WHERE user_id = :user');
		$stat->bindParam(':user', $user);
		$stat->execute();
		$files = $stat->fetchAll();
		$c = null;
		echo '{"return":"true",  "message":"Arquivos retornados", "info":{"files":'.json_encode($files).'}}';
	} catch (Exception $e) {
		echo json_encode(array('return'=>'false', 'message'=>$e->getMessage()));
	}
}

function selectFile($app, $id){
	require_once 'lib/database.php';

	$user = getUser($app);
	if(!$user){
		echo '{"return":"false", "message":"Usuario nao encontrado."}';
		die();
	}
	$user = $user[0]['id'];
	
	try {
		$c 	  = getConnection();
		$stat = $c->prepare('SELECT * FROM files WHERE id = :id AND user_id = :user');
		$stat->bindParam(':id',   $id);
		$stat->bindParam(':user', $user);
		$stat->execute();
		$file = $stat->fetchAll();
		$c = null;
		#echo '{"return":"false", "message":"'.$id.','.$user.'"}';
		#die();
		echo '{"return":"true",  "message":"Arquivos retornados", "info":{"files":'.json_encode($file).'}}';
	} catch (Exception $e) {
		echo json_encode(array('return'=>'false', 'message'=>$e->getMessage()));
	}
}

function fileExists($app, $name, $location, $user){
	require_once 'lib/database.php';

	try {
		$c 	  = getConnection();
		$stat = $c->prepare('SELECT * FROM files WHERE name = :name AND location = :location AND user_id = :user');
		$stat->bindParam(':name',   	$name);
		$stat->bindParam(':location',   $location);
		$stat->bindParam(':user',		$user);
		$stat->execute();
		$file = $stat->fetchAll();
		$c = null;
		
		if(count($file) == 1){
			return $file[0]['id'];
		}else{
			return false;
		}
	} catch (Exception $e) {
		echo json_encode(array('return'=>'false', 'message'=>$e->getMessage()));
	}
	return false;
}

//curl -X DELETE http://localhost/slim/index.php/arquivo/delete/6
function deleteFile($app){
	require_once 'lib/database.php';
	
	$user = getUser($app);
	
	if(!$user){
		echo '{"return":"false", "message":"Usuario nao encontrado."}';
		die();
	}
	
	$user 	 = $user[0]['id'];
	$name  	 = $app->request()->post('name');
	$location= $app->request()->post('location');
	$status  = $app->request()->post('status');
	$deleteType  	 = $app->request()->post('deleteType');
	
	if(!fileExists($app, $name, $location, $user)){
		echo json_encode(array('return'=>'true', 'message'=>'arquivo nao existe'));
		die();
	}
	
	//soft delete
	if($deleteType == 0){

		try {
			$c = getConnection();
			$stat = $c->prepare('UPDATE files SET status_id = :status, modified = NOW() WHERE name = :name AND location = :location AND user_id = :user');
			$stat->bindParam(':status', 	$status);
			$stat->bindParam(':user', 		$user);
			$stat->bindParam(':name', 		$name);
			$stat->bindParam(':location', 	$location);
			$stat->execute();
		
			if($stat->rowCount() == 1){
				echo json_encode(array('return'=>'true', 'message'=>'Arquivo alterado'));
			}else{
				echo json_encode(array('return'=>'false', 'message'=>'Falha ao alterar arquivo'));
			}
		
			$c = null;
		} catch (Exception $e) {
			echo json_encode(array('return'=>'false', 'message'=>$e->getMessage()));
		}
		
	//hard delete
	}elseif ($deleteType == 1){
	
		try {
			$c = getConnection();
			$stat = $c->prepare('DELETE FROM files WHERE name = :name AND location = :location AND user_id = :user');
			$stat->bindParam(':user', 		$user);
			$stat->bindParam(':name', 		$name);
			$stat->bindParam(':location', 	$location);
			$stat->execute();
			$c = null;
			echo json_encode(array('return'=>'true', 'message'=>'Arquivo deletado'));
		} catch (Exception $e) {
			echo json_encode(array('return'=>'false', 'message'=>$e->getMessage()));
		}
		
	}
}
//curl -X POST -d "nome=novo.txt" -d "local=local/arquivo" http://localhost/slim/index.php/arquivo
function addFile($app){
	require_once 'lib/database.php';

	$user = getUser($app);
	
	if(!$user){
		echo '{"return":"false", "message":"Usuario nao encontrado."}';
		die();
	}
	
	$name    	= $app->request()->post('name');
	$location 	= $app->request()->post('location');
	$userId 	= $user[0]['id'];
	$status  	= $app->request()->post('status');
	
	$file = fileExists($app, $name, $location, $userId);
	
	if($file){
		if(updateFile($app, $name, $location, $userId)){
			echo json_encode(array(
					"return"	=>	"true",
					"message"	=>	"Arquivo atualizado",
					"info"		=>	array("id"=>$file, "user"=>array(
						"id" 	=>	$userId,
						"login" =>	$user[0]['login']
					))
			));
		}else{
			echo json_encode(array(
					"return"	=>	"false",
					"message"	=>	"Arquivo existente, mas não foi possivel atualiza-lo",
					"info"		=>	array("id"=>$id, "user"=>array(
							"id" 	=>	$userId,
							"login" =>	$user[0]['login']
					))
			));
		}
	}else{
		try {
			$c = getConnection();
			$stat = $c->prepare('INSERT INTO files VALUES (null, :name, :location, NOW(), NOW(), :user, :status)');
			$stat->bindParam(':name', 		$name);
			$stat->bindParam(':location', 	$location);
			$stat->bindParam(':user', 		$userId);
			$stat->bindParam(':status', 	$status);
			$stat->execute();
			$id = $c->lastInsertId();
			
			if($stat->rowCount() == 1){
				echo json_encode(array(
						"return"	=>	"true",
						"message"	=>	"Novo arquivo inserido",
						"info"		=>	array("id"=>$id, "user"=>array(
							"id" 	=>	$userId,
							"login" =>	$user[0]['login']
						))
				));
			}else{
				echo '{"return":"false", "message":"falha ao adicionar arquivo"}';
			}
	
			$c = null;
		} catch (Exception $e) {
			echo json_encode(array('return'=>'false', 'message'=>$e->getMessage()));
		}
		
	}
}
//curl -X PUT -d "nome=novo2.txt" -d "local=local/arquivo" http://localhost/slim/index.php/arquivo/8
function updateFileStatus($app){
	require_once 'lib/database.php';

	$user = getUser($app);
	
	if(!$user){
		echo '{"return":"false", "message":"Usuario nao encontrado."}';
		die();
	}
	
	$id  	 = $app->request()->post('id');
	$user 	 = $user[0]['id'];
	$status  = $app->request()->post('status');
	
	try {
		$c = getConnection();
		$stat = $c->prepare('UPDATE files SET status_id = :status, modified = NOW() WHERE id = :id AND user_id = :user');
		$stat->bindParam(':status', $status);
		$stat->bindParam(':user', $user);
		$stat->bindParam(':id', $id);
		$stat->execute();

		if($stat->rowCount() == 1){
			echo '{"return":"true",  "message":"arquivo alterado"}';
		}else{
			echo '{"return":"false", "message":"falha ao alterar arquivo"}';
		}

		$c = null;
	} catch (Exception $e) {
		echo json_encode(array('return'=>'false', 'message'=>$e->getMessage()));
	}
}

function updateFile($app, $name, $location, $userId){
	require_once 'lib/database.php';
	
	try {
		$c = getConnection();
		$stat = $c->prepare("UPDATE files SET modified = NOW() WHERE name = :name AND location = :location AND user_id = :user");
		$stat->bindParam(':name', 		$name, 		PDO::PARAM_STR);
		$stat->bindParam(':location', 	$location, 	PDO::PARAM_STR);
		$stat->bindParam(':user', 		$userId, 	PDO::PARAM_INT);
		$stat->execute();

		if($stat->rowCount() == 1){
			return true;
		}else{
			return false;
		}
		
		$c = null;
	} catch (Exception $e) {
		echo json_encode(array('return'=>'false', 'message'=>$e->getMessage()));
		die();
	}
}

function addSchedule($app){
	$user = getUser($app);
	
	if(!$user){
		echo '{"return":"false", "message":"Usuario nao encontrado."}';
		die();
	}
	
	$directory    	= $app->request()->post('directory');
	$interval 		= $app->request()->post('interval');
	$status  		= $app->request()->post('status');
	$userId  		= $user[0]['id'];
	
	try {
		$c = getConnection();
		$stat = $c->prepare('INSERT INTO schedules VALUES (null, :directory, :interval, NOW(), :user, :status)');
		$stat->bindParam(':directory', 	$directory);
		$stat->bindParam(':interval', 	$interval);
		$stat->bindParam(':user', 		$userId);
		$stat->bindParam(':status', 	$status);
		$stat->execute();
		$id = $c->lastInsertId();
			
		if($stat->rowCount() == 1){
			echo json_encode(array(
					"return"	=>	"true",
					"message"	=>	"Novo agendamento inserido",
					"info"		=>	array("id"=>$id, "user"=>array(
							"id" 	=>	$userId,
							"login" =>	$user[0]['login']
					))
			));
		}else{
			//$str = 'INSERT INTO schedules VALUES (null, '.$directory.', '.$interval.', NOW(), '.$user.', '.$status.')';
			echo '{"return":"false", "message":"falha ao adicionar agendamento"}';
		}

		$c = null;
	} catch (Exception $e) {
		echo json_encode(array('return'=>'false', 'message'=>$e->getMessage()));
	}
}

function selectSchedules($app){
	$user = getUser($app);
	
	if(!$user){
		echo '{"return":"false", "message":"Usuario nao encontrado."}';
		die();
	}
	
	$user = $user[0]['id'];
	
	try {
		$c 	  = getConnection();
		$stat = $c->prepare('SELECT * FROM schedules WHERE user_id = :user');
		$stat->bindParam(':user', $user);
		$stat->execute();
		$schedules = $stat->fetchAll();
		$c = null;
		echo json_encode(array('return'=>'true', 'schedules' => json_encode($schedules)));
	} catch (Exception $e) {
		echo json_encode(array('return'=>'false', 'message'=>$e->getMessage()));
	}
}

function selectSchedule($app, $id){
	
	$user = getUser($app);
	if(!$user){
		echo '{"return":"false", "message":"Usuario nao encontrado."}';
		die();
	}
	$user = $user[0]['id'];
	
	try {
		$c 	  = getConnection();
		$stat = $c->prepare('SELECT * FROM schedules WHERE id = :id AND user_id = :user');
		$stat->bindParam(':id',   $id);
		$stat->bindParam(':user', $user);
		$stat->execute();
		$file = $stat->fetchAll();
		$c = null;

		echo '{"return":"true",  "message":"Agendamentos retornados", "info":{"schedules":'.json_encode($file).'}}';
	} catch (Exception $e) {
		echo json_encode(array('return'=>'false', 'message'=>$e->getMessage()));
	}
}

function updateSchedule($app){
	$user = getUser($app);
	
	if(!$user){
		echo '{"return":"false", "message":"Usuario nao encontrado."}';
		die();
	}
	
	$id  	 	= $app->request()->post('id');
	$directory  = $app->request()->post('directory');
	$interval 	= $app->request()->post('interval');
	$status  	= $app->request()->post('status');
	$last_backup = $app->request()->post('last_backup');
	$userId  	= $user[0]['id'];
	
	try {
		$c = getConnection();
		$stat = $c->prepare('UPDATE `schedules` SET `directory` = :directory, `interval` = :interval, `status` = :status, `last_backup` = :last_backup WHERE `id` = :id AND `user_id` = :user');
		$stat->bindParam(':directory', 	$directory);
		$stat->bindParam(':interval', 	$interval);
		$stat->bindParam(':status', 	$status);
		$stat->bindParam(':last_backup',$last_backup);
		$stat->bindParam(':id', 		$id);
		$stat->bindParam(':user', 		$userId);
		$stat->execute();

		//$str = 'UPDATE `schedules` SET `directory` = '.$directory.', `interval` = '.$interval.', `status` = '.$status.' WHERE `id` = '.$id.' AND `user_id` = '.$userId;
		
		if($stat->rowCount() == 1){
			echo json_encode(array(
					"return"	=>	"true",
					"message"	=>	"Agendamento atualizado",
			));
		}else{
			//echo '{"return":"false", "message":"'.$str.'"}';
			echo '{"return":"false", "message":"Falha ao atualizar agendamento"}';
		}
	
		$c = null;
	} catch (Exception $e) {
		echo json_encode(array('return'=>'false', 'message'=>$e->getMessage()));
	}
}

function updateScheduleLastBackup($app){
	$user = getUser($app);

	if(!$user){
		echo '{"return":"false", "message":"Usuario nao encontrado."}';
		die();
	}

	$id  	 		= $app->request()->post('id');
	$last_backup 	= $app->request()->post('last_backup');
	$userId  		= $user[0]['id'];

	try {
		$c = getConnection();
		$stat = $c->prepare('UPDATE `schedules` SET  `last_backup` = NOW() WHERE `id` = :id AND `user_id` = :user');
		$stat->bindParam(':id', 		$id);
		$stat->bindParam(':user', 		$userId);
		$stat->execute();

		//$str = 'UPDATE `schedules` SET  WHERE `id` = '.$id.' AND `user_id` = '.$userId;

		if($stat->rowCount() == 1){
			echo json_encode(array(
					"return"	=>	"true",
					"message"	=>	"Ultimo backup do agendamento atualizado",
			));
		}else{
			//echo '{"return":"false", "message":"'.$str.'"}';
			echo '{"return":"false", "message":"Falha ao atualizar ultimo backup do agendamento"}';
		}

		$c = null;
	} catch (Exception $e) {
		echo json_encode(array('return'=>'false', 'message'=>$e->getMessage()));
	}
}

function deleteSchedule($app){
	require_once 'lib/database.php';
	
	$user = getUser($app);
	
	if(!$user){
		echo '{"return":"false", "message":"Usuario nao encontrado."}';
		die();
	}
	
	$id  	 = $app->request()->post('id');
	$user 	 = $user[0]['id'];
	
	try {
		$c = getConnection();
		$stat = $c->prepare('DELETE FROM schedules WHERE id = :id AND user_id = :user');
		$stat->bindParam(':id',   $id);
		$stat->bindParam(':user', $user);
		
		$stat->execute();
		$c = null;
		echo json_encode(array('return'=>'true', 'message'=>'Agendamento deletado'));
	} catch (Exception $e) {
		echo json_encode(array('return'=>'false', 'message'=>$e->getMessage()));
	}
}

function selectSchedulesToBackup($app){
	/*
	 	SELECT *
		FROM `schedules`
		WHERE ADDDATE( `last_backup` , INTERVAL `interval` HOUR ) <= NOW()
		LIMIT 0 , 30
	*/
	$user = getUser($app);
	
	if(!$user){
		echo '{"return":"false", "message":"Usuario nao encontrado."}';
		die();
	}
	
	$user = $user[0]['id'];
	
	try {
		$c 	  = getConnection();
		$stat = $c->prepare('SELECT * FROM `schedules` WHERE `user_id`= :user AND ADDDATE( `last_backup` , INTERVAL `interval` HOUR ) <= NOW()');
		$stat->bindParam(':user', $user);
		$stat->execute();
		$schedules = $stat->fetchAll();
		$c = null;
		echo json_encode(array('return'=>'true', 'schedules' => json_encode($schedules)));
	} catch (Exception $e) {
		echo json_encode(array('return'=>'false', 'message'=>$e->getMessage()));
	}
	 
}

$app->run();