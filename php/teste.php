<?php

include 'password.php';

$password = "password";

$hash = password_hash($password, PASSWORD_BCRYPT);

//$hash = '$2a$12$HHQkGiv.HzQ3bj12eqExKOOma0YTGc3T7CZhUs0mkPSEJ8vdeMBLy';
//$hash = '$2a$12$pFTaoDQ4a/RmVVDFonnqnOUqZN7bzYbboHGtKFFRLxgEI.r6lPEyK';

if (password_verify($password, $hash)) {
	echo "valido";
} else {
	echo "invalido";
}