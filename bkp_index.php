<?php

require 'Slim/Slim.php';
require 'Slim/Middleware.php';
require 'Slim/Extras/Middleware/HttpBasicAuth.php';
require 'Slim/Extras/Middleware/HttpDigestAuth.php';
//require 'Slim/Extras/Middleware/teste.php';

\Slim\Slim::registerAutoloader();

//use \Slim\Extras\Middleware\HttpBasicAuth;
//use \Slim\Extras\Middleware\HttpDigestAuth;

$app = new \Slim\Slim();

//$app->add(new HttpDigestAuth(array('username' => 'user', 'password' => 'passwd')));
$app->add(new HttpBasicAuth('user', 'password'));

$app->get('/', 		 'teste');
$app->get('/arquivos', 		 'getArquivos');
$app->get('/arquivo/:id', 	 'getArquivo');
$app->post('/arquivo/', 	 function()    use($app) { addArquivo($app);         });
$app->put('/arquivo/:id', 	 function($id) use($app) { updateArquivo($app, $id); });
$app->delete('/arquivo/:id', 'deleteArquivo');

/*
 * HttpBasicAuth 	=> curl -i -X GET  --user user:passwd http://localhost/webservice/index.php/arquivos
 * HttpDigestAuth 	=> 
 */

function teste(){
	echo '{"msg":'.$_SERVER['SCRIPT_NAME'].'}';
}

function getArquivos(){
	
	require_once 'lib/database.php';
	$query = 'SELECT * FROM arquivos';
	
	try {
		$c = getConnection();
		$stat = $c->query($query);
		$arquivos = $stat->fetchAll(PDO::FETCH_OBJ);
		$c = null;	
		echo '{"arquivos":'.json_encode($arquivos).'}';
	} catch (Exception $e) {
		echo "erro";
		echo '{"error"{"text":'.$e->message().'}}';
	}
}

function getArquivo($id){
	require_once 'lib/database.php';

	try {
		$c = getConnection();
		$stat = $c->prepare('SELECT * FROM arquivos WHERE id = :id');
		$stat->bindParam(':id', $id);
		$stat->execute();
		$arquivos = $stat->fetchAll();
		$c = null;
		echo '{"arquivos":'.json_encode($arquivos).'}';
	} catch (Exception $e) {
		echo '{"error"{"text":'.$e->message().'}}';
	}
}

//curl -X DELETE http://localhost/slim/index.php/arquivo/delete/6
function deleteArquivo($id){
	require_once 'lib/database.php';
	try {
		$c = getConnection();
		$stat = $c->prepare('DELETE FROM arquivos WHERE id = :id');
		$stat->bindParam(':id', $id);
		$stat->execute();
		$c = null;
		echo '{"arquivo deletado:":'.$id.'}';
	} catch (Exception $e) {
		echo '{"error"{"text":'.$e->message().'}}';
	}
}
//curl -X POST -d "nome=novo.txt" -d "local=local/arquivo" http://localhost/slim/index.php/arquivo
function addArquivo($app){
	require_once 'lib/database.php';

	$nome  = $app->request()->post('nome');
	$local = $app->request()->post('local');
	
	try {
		$c = getConnection();
		$stat = $c->prepare('INSERT INTO arquivos VALUES (null, :nome, :local, NOW(), NOW())');
		$stat->bindParam(':nome', $nome);
		$stat->bindParam(':local', $local);
		$stat->execute();
		
		if($stat->rowCount() == 1){
			echo '{"retorno":"true",  "msg":"Novo arquivo inserido, id = '.$c->lastInsertId().'"}';
		}else{
			echo '{"retorno":"false", "msg":"falha ao adicionar arquivo"}';
		}
		
		$c = null;
	} catch (Exception $e) {
		echo '{"error"{"text":'.$e->message().'}}';
	}
}
//curl -X PUT -d "nome=novo2.txt" -d "local=local/arquivo" http://localhost/slim/index.php/arquivo/8
function updateArquivo($app, $id){
	require_once 'lib/database.php';
	
	$nome  = $app->request()->post('nome');
	$local = $app->request()->post('local');
	
	try {
		$c = getConnection();
		$stat = $c->prepare('UPDATE arquivos SET nome = :nome, local = :local, modificado = NOW() WHERE id = :id');
		$stat->bindParam(':nome', $nome);
		$stat->bindParam(':local', $local);
		$stat->bindParam(':id', $id);
		$stat->execute();
		
		if($stat->rowCount() == 1){
			echo '{"retorno":"true",  "msg":"arquivo alterado"}';
		}else{
			echo '{"retorno":"false", "msg":"falha ao alterar arquivo"}';
		}
		
		$c = null;
	} catch (Exception $e) {
		echo '{"error"{"text":'.$e->message().'}}';
	}
}

$app->run();