<?php
require 'Slim/Slim.php';
require 'Slim/Middleware.php';
require 'Slim/Extras/Middleware/HttpBasicAuth.php';



\Slim\Slim::registerAutoloader();

//use \Slim\Extras\Middleware\HttpBasicAuth;

$app = new \Slim\Slim();

$app->add(new HttpBasicAuth('user', 'password'));

$app->get('/teste', 		 'teste');
$app->get('/arquivos', 		 'getArquivos');
$app->get('/arquivo/:id', 	 'getArquivo');
$app->post('/arquivo/', 	 function()    use($app) { addArquivo($app);         });
$app->put('/arquivo/:id', 	 function($id) use($app) { updateArquivo($app, $id); });
$app->delete('/arquivo/:id', 'deleteArquivo');
$app->post('/user/', 	 	 function()    use($app) { addUser($app);            });
//$app->post('/auth/', 	 	 function()    use($app) { auth($app);            	 });

/*
 * HttpBasicAuth 	=> curl -i -X GET  --user user:passwd http://localhost/webservice/index.php/arquivos
 * HttpDigestAuth 	=>
 */

//curl -X POST -d "login=login" -d "password=password" http://localhost/slim/index.php/user
function addUser($app){
	require_once 'lib/database.php';
	
	$login  	= $app->request()->post('login');
	$password 	= $app->request()->post('password');
	
	try {
		$c = getConnection();
		$stat = $c->prepare('INSERT INTO users VALUES (NULL, :login, :password)');
		$stat->bindParam(':login',  $login);
		$stat->bindParam(':password', $password);
		$s=$stat->execute();
	
		if($stat->rowCount() == 1){
			echo '{"retorno":"true",  "msg":"Novo usuario inserido, id = '.$c->lastInsertId().'"}';
		}else{
			echo '{"retorno":"false", "msg":"falha ao adicionar usuario. Dados enviados: login: '.$login.', password: '.$password.'"}';
		}
	
		$c = null;
	} catch (Exception $e) {
		echo '{"error"{"text":'.$e.'}}';
	}
}

//metodo desnecessario, pois cada requisição contem as credenciais
function auth($app){
	require_once 'lib/database.php';
	
	$login  	= $app->request()->post('login');
	$password 	= $app->request()->post('password');
	
	try {
		$c = getConnection();
		$stat = $c->prepare('SELECT * FROM users WHERE login = :login');
		$stat->bindParam(':login', 	  $login);
		
		$stat->execute();
		$user = $stat->fetchAll();
		$c = null;
		
		if(count($user) == 1){
			if(password_verify($password, $user[0]['password'])){
				echo '{"retorno":"true",  "msg":OK, '.json_encode($user).'}';
			}else{
				echo '{"retorno":"false", "msg":usuario nao autenticado. Dados enviados: login: '.$login.', password: '.$password.', hash: '.$user[0]['password'].'}';
			}
		}else{
			echo '{"retorno":"false", "msg":usuario nao encontrado. Dados enviados: login: '.$login.', password: '.$password.'}';
		}
		
	} catch (Exception $e) {
		echo '{"error"{"text":'.$e.'}}';
	}
}

function teste(){
	echo '{"msg":'.$_SERVER['SCRIPT_NAME'].'}';
}

function getArquivos(){
	require_once 'lib/database.php';
	$query = 'SELECT * FROM arquivos';

	try {
		$c = getConnection();
		$stat = $c->query($query);
		$arquivos = $stat->fetchAll(PDO::FETCH_OBJ);
		$c = null;
		echo '{"arquivos":'.json_encode($arquivos).'}';
	} catch (Exception $e) {
		echo "erro";
		echo '{"error"{"text":'.$e->message().'}}';
	}
}

function getArquivo($id){
	require_once 'lib/database.php';

	try {
		$c = getConnection();
		$stat = $c->prepare('SELECT * FROM arquivos WHERE id = :id');
		$stat->bindParam(':id', $id);
		$stat->execute();
		$arquivos = $stat->fetchAll();
		$c = null;
		echo '{"arquivos":'.json_encode($arquivos).'}';
	} catch (Exception $e) {
		echo '{"error"{"text":'.$e->message().'}}';
	}
}

//curl -X DELETE http://localhost/slim/index.php/arquivo/delete/6
function deleteArquivo($id){
	require_once 'lib/database.php';
	try {
		$c = getConnection();
		$stat = $c->prepare('DELETE FROM arquivos WHERE id = :id');
		$stat->bindParam(':id', $id);
		$stat->execute();
		$c = null;
		echo '{"arquivo deletado:":'.$id.'}';
	} catch (Exception $e) {
		echo '{"error"{"text":'.$e->message().'}}';
	}
}
//curl -X POST -d "nome=novo.txt" -d "local=local/arquivo" http://localhost/slim/index.php/arquivo
function addArquivo($app){
	require_once 'lib/database.php';

	$nome  = $app->request()->post('nome');
	$local = $app->request()->post('local');

	try {
		$c = getConnection();
		$stat = $c->prepare('INSERT INTO arquivos VALUES (null, :nome, :local, NOW(), NOW())');
		$stat->bindParam(':nome', $nome);
		$stat->bindParam(':local', $local);
		$stat->execute();

		if($stat->rowCount() == 1){
			echo '{"retorno":"true",  "msg":"Novo arquivo inserido, id = '.$c->lastInsertId().'"}';
		}else{
			echo '{"retorno":"false", "msg":"falha ao adicionar arquivo"}';
		}

		$c = null;
	} catch (Exception $e) {
		echo '{"error"{"text":'.$e->message().'}}';
	}
}
//curl -X PUT -d "nome=novo2.txt" -d "local=local/arquivo" http://localhost/slim/index.php/arquivo/8
function updateArquivo($app, $id){
	require_once 'lib/database.php';

	$nome  = $app->request()->post('nome');
	$local = $app->request()->post('local');

	try {
		$c = getConnection();
		$stat = $c->prepare('UPDATE arquivos SET nome = :nome, local = :local, modificado = NOW() WHERE id = :id');
		$stat->bindParam(':nome', $nome);
		$stat->bindParam(':local', $local);
		$stat->bindParam(':id', $id);
		$stat->execute();

		if($stat->rowCount() == 1){
			echo '{"retorno":"true",  "msg":"arquivo alterado"}';
		}else{
			echo '{"retorno":"false", "msg":"falha ao alterar arquivo"}';
		}

		$c = null;
	} catch (Exception $e) {
		echo '{"error"{"text":'.$e->message().'}}';
	}
}
	
$app->run();