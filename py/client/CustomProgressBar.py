from PySide.QtGui import *
from PySide.QtCore import *

class CustomProgressBar(QProgressBar, QObject):
    sigInit     = Signal(int)
    sigDestroy  = Signal()
    sigUpdate   = Signal()
    
    def __init__(self):
        QProgressBar.__init__(self) 
        #QObject.__init__(self)
        
        self.sigInit.connect(self.initProgressBar)
        self.sigDestroy.connect(self.destroyProgressBar)
        self.sigUpdate.connect(self.upadateProgressBar)
    
    def initProgressBar(self, int):
        self.setMinimum(0)
        self.setMaximum(int)
        self.setValue(0)
        self.show()
    
    def destroyProgressBar(self):
        self.hide()
    
    def upadateProgressBar(self):
        self.setValue(self.value()+1)
    
    
