import Request
import os
import base64
import requests
from requests.auth import HTTPBasicAuth, HTTPDigestAuth

url     = 'http://localhost/webservice/index.php/schedulesToBackup'
user    = base64.b64encode('dev')
passwd  = base64.b64encode('guloseimas')

data    = {'directory':'/', 'interval':'2', 'status':0, 'id':4}

request = requests.get(url, auth=HTTPBasicAuth(user, passwd), data=data)

print request.json()