# -*- coding: UTF-8 -*-
import sys, time, os
from PySide.QtGui import *
from PySide.QtCore import *
import requests
import bcrypt
import Request
import base64
import json
from requests.auth import HTTPBasicAuth, HTTPDigestAuth
import platform
from crontab import CronTab

try:
    import winpaths
except:
    pass

class Schedule(QDialog):
    
    ftp         = None
    user        = ''
    password    = ''
    directory   = ''
    interval    = ''
    last_backup = ''
    status      = ''
    schedules   = []
    THDict      = {'directory':'Diretorio', 'interval':'Intervalo', 'last_backup':'Ultimo backup'}
    TableHeaders= ['Diretorio', 'Intervalo', 'Ultimo backup']
    intervals   = {'1 hora':1, '2 horas':2, '4 horas':4, '8 horas':8, '12 horas':12, '24 horas':24}
        
    def __init__(self, user, password):
        
        self.user = user
        self.password = password
        
        QDialog.__init__(self)
        layout = QHBoxLayout()
        
        layout.addLayout(self.setupScheduleForm())
        layout.addLayout(self.setupTable())
        
        self.setWindowTitle("Agendar")
        self.setWindowIcon(QIcon('img/icon.png'))
        
        self.setLayout(layout)
        self.refreshTable()
        self.show()
        
    def connect(self):
        if self.ftp == None or self.ftp.isClosed():
            self.ftp = Ftp(self.usuario, self.password)
            self.ftp.connect()
        
    def setupScheduleForm(self):
        layout       = QVBoxLayout()
        formLayout   = QFormLayout()
        
        labelTime      = QLabel('Intervalo', self)
        self.comboTime = QComboBox(self)
        
        for time in self.intervals:
            self.comboTime.addItem(time)
        
        labelDir    = QLabel('Diretorio', self)
        self.txtDir = QLineEdit(self)
        self.txtDir.setReadOnly(True)
        
        labelBtnDir       = QLabel('', self)
        self.btnSelectDir = QPushButton('Selecionar diretorio',self)
        self.btnSelectDir.clicked.connect(self.showDialogDir)

        labelSubmit    = QLabel('', self)
        self.btnSubmit = QPushButton('Agendar',self)
        self.btnSubmit.clicked.connect(self.agendar)
        
        formLayout.addRow(labelTime,   self.comboTime)
        formLayout.addRow(labelDir,    self.txtDir)
        formLayout.addRow(labelBtnDir, self.btnSelectDir)
        formLayout.addRow(labelSubmit, self.btnSubmit)
        
        layout.addLayout(formLayout)
        
        return layout
    
    def setupTable(self):
        layout = QVBoxLayout()
        layout2 = QHBoxLayout()
        labelTable = QLabel('Backups agendados:', self)
        
        self.table = QTableWidget(0, len(self.TableHeaders), self)
        self.table.setHorizontalHeaderLabels(self.TableHeaders)
        self.table.resizeColumnsToContents()
        self.table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.table.setEditTriggers(False)
        #layout.addSpacing(200)
        layout.addWidget(labelTable)
        layout.addWidget(self.table)
        
        btnDelSchedule = QPushButton('Deletar',self)
        btnDelSchedule.clicked.connect(self.deleteSchedule)
        
        btnUpdateTable = QPushButton('Atualizar',self)
        btnUpdateTable.clicked.connect(self.refreshTable)
        
        layout2.addWidget(btnDelSchedule)
        layout2.addWidget(btnUpdateTable)
        layout.addLayout(layout2)
        #layout.addLayout(self.setupTableButtons())
        
        return layout
    
    def showDialogDir(self):
        dialog = QFileDialog(self)
        dialog.setDirectory('/var/www/html/webservice/py/interface')
        dialog.setFileMode(QFileDialog.Directory);
        
        if dialog.exec_():
            try:
                path = dialog.selectedFiles();
                self.txtDir.setText(path[0])
                self.directory = path[0]
                
            except Exception, e:
                print "Erro ao selecionar diretorio, ", str(e)
             
    def deleteSchedule(self):
        try:
            row = self.table.currentRow()
            id  = self.schedules[row]['id']
            
            url     = Request.urls['deleteSchedule']
            user    = base64.b64encode(self.user)
            passwd  = base64.b64encode(self.password)
            
            data = {'id':id}

            request = requests.delete(url, auth=HTTPBasicAuth(user, passwd), data=data)
            
            if(Request.handleResponse(request)):
                del self.schedules[row]
                self.updateTable()
                return True
            else:
                print "Nao foi possivel deletar o agendamento"
                return False

        except Exception, e:
            print "Erro ao deletar agendamento", str(e)
                    
    def agendar(self):
        try:
            comboVal = self.comboTime.currentText()
            interval = self.intervals[comboVal]
            self.interval = interval
            
            if not self.directory:
                QMessageBox.about(self, "Alerta",u"Escolha um diretório para ser feito o backup, clicando em 'Selecionar diretório'")
            elif not self.interval:
                QMessageBox.about(self, "Alerta",u"Escolha um intervalo de tempo para ser feito o backup")
            else:
                url     = Request.urls['addSchedule']
                user    = base64.b64encode(self.user)
                passwd  = base64.b64encode(self.password)
                data    = {'directory': self.directory, 'interval':self.interval, 'status':'1'}
                
                request = requests.post(url, auth=HTTPBasicAuth(user, passwd), data=data)
                
                if(Request.handleResponse(request)):
                    self.selectSchedules()
                    self.updateTable()
                    self.saveConfigFile()
                    self.saveAutoRunFile()
                    return True
                else:
                    print "Nao foi possivel agendar o backup"
                    return False
            
        except Exception, e:
            print "Erro ao agendar, ", str(e)
            
    def formatSchedules(self):
        for s in self.schedules:
            interval = s['interval']+' horas'
            s['interval'] = interval
            
    def selectSchedules(self):
        try:
            url     = Request.urls['selectSchedules']
            user    = base64.b64encode(self.user)
            passwd  = base64.b64encode(self.password)
            
            request = requests.get(url, auth=HTTPBasicAuth(user, passwd))
            
            if(Request.handleResponse(request)):
                j = request.json()
                self.schedules = json.loads(j['schedules'])
                self.formatSchedules()
                return True
            else:
                print "Nao foi possivel selecionar os agendamentos"
                return False
        
        except Exception, e:
            print "Erro ao selecionar agendamentos", str(e)
            
    def updateTable(self):
        try:
            self.table.clearContents()
            
            columnCount = self.table.columnCount()
            row         = 0
            column      = 0
            
            rowCount = len(self.schedules)
            
            self.table.setRowCount(rowCount)
            
            for i in self.schedules:
                while (column < columnCount):
                    for t in self.THDict:
                        self.table.setItem(row, column, QTableWidgetItem(i[t]))
                        i['Row'] = row
                        column += 1
                row += 1
                column = 0
                
        except Exception, e:
            print 'Error ao atualizar tabela,', str(e)
            
    def refreshTable(self):
        try:
            self.selectSchedules()
            self.updateTable()
        except Exception, e:
            print 'Error ao atualizar tabela,', str(e)
        
    def saveConfigFile(self):
        try:
            creedences = {0:base64.b64encode(self.user), 1:base64.b64encode(self.password)}
            fo = open("config.txt", "wb")
            fo.write(str(creedences));
            fo.close()
        except Exception, e:
            print 'Error ao salvar arquivo de configuracao,', str(e)
        
    def saveAutoRunFile(self):
        try:
            plataforma = platform.platform()
            versao = platform.release()
            
            currentFolder = os.path.dirname(os.path.abspath(__file__))
            print currentFolder
            
            if plataforma.startswith("Linux"):
                
                file = currentFolder+"/runSync.bat"
                
                tab = CronTab()
                
                job  = tab.new(command='python '+currentFolder+"/Sync/Sync.py")
                job.hour.every(1)
                tab.write()
                print tab.render()
                """
                file = currentFolder+"/runSync.bat"
                
                f = open(file, "wb")
                f.write( 'start "" "'+currentFolder+'/sync/Sync"');
                f.close()
                """
            elif plataforma.startswith("Windows"):
                
                startupFolder = winpaths.get_startup()
                file = startupFolder+"\\runSync.bat"
                
                f = open(file, "wb")
                f.write( 'start "" "'+currentFolder+'\\sync\\Sync"');
                f.close()
                
        except Exception, e:
            print "erro ao salvar auto run file", e
    
if __name__ == '__main__':
    myApp = QApplication(sys.argv)
    widget = Schedule('dev','guloseimas')
    widget.show()
    widget.saveConfigFile()
    widget.selectSchedules()
    widget.updateTable()
    myApp.exec_()
    sys.exit(0)