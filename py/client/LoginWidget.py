import sys, time, os
from MainWindow import MainWindow
from Widget import Widget
from PySide.QtGui import *
from PySide.QtCore import *
#sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../ftp'))
from User2 import User2

class LoginWidget(QWidget):
    
    mainWindow = ''
    widget = ''
    
    def __init__(self, mainWindow, widget):
        QWidget.__init__(self)
        
        self.mainWindow = mainWindow
        self.widget     = widget
        
        self.setWindowTitle("Login")
        self.setWindowIcon(QIcon('img/icon.png'))
        self.setGeometry(600, 250, 600, 100)

        self.setupLayout()
        
    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Enter or event.key() == 16777220:
            self.login()
        if event.key() == Qt.Key_Escape:
            self.close()
        
    def setupLayout(self):
        layout = QVBoxLayout()
        
        layout.addLayout(self.setupLoginForm())
        
        self.setLayout(layout)
        
    def setupLoginForm(self):
        layout = QHBoxLayout()
        formLayout = QFormLayout()
        
        labelUser = QLabel('Usuario', self)
        self.txtUser = QLineEdit(self)
        labelPassword = QLabel('Senha', self)
        self.txtPassword = QLineEdit(self)
        self.txtPassword.setEchoMode(QLineEdit.Password)
        
        self.btnLogin = QPushButton('Login',self)
        self.btnLogin.setMaximumWidth(150)
        self.btnLogin.clicked.connect(self.login)
        
        
        #btnSair = QPushButton('Sair',self)
        #btnSair.setMaximumWidth(150)
        #btnSair.clicked.connect(self.login)
        
        formLayout.addRow(labelUser,     self.txtUser)
        formLayout.addRow(labelPassword, self.txtPassword)
        formLayout.addWidget(self.btnLogin)
        #formLayout.addWidget(btnSair)
        
        layout.addLayout(formLayout)
        
        return layout
   
    def login(self):
        try:
            self.btnLogin.setDisabled(1)
            
            usuario = self.txtUser.text()
            password = self.txtPassword.text()
            user = User2(usuario, password, 0)
            auth = user.auth()
            if auth == 'true':
                self.openWidget(usuario, password)
            else:
                QMessageBox.about(self, "Login","Usuario ou senha invalido.")
                self.btnLogin.setDisabled(0)
        except Exception, e:
            print 'Error ao realizar login,', str(e)
        
    def openWidget(self, usuario, password):
        try:
            self.widget.usuario  = usuario
            self.widget.password = password
            
            self.mainWindow.usuario  = usuario
            self.mainWindow.password = password
            self.widget.path     = '/home/'+usuario
            
            #self.widget.path     = 'testes/users/ricardo'
            
            self.mainWindow.show()
            self.widget.show()
            self.widget.updateList()
            self.hide()
        except Exception, e:
            print 'Error ao abrir widget,', str(e)
        
if __name__ == '__main__':
    myApp = QApplication(sys.argv)
    widget = LoginWidget()
    widget.show()
    myApp.exec_()
    sys.exit(0)