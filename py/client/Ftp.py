from ftplib import FTP_TLS
import ftputil
import os
from time import sleep
from settings import settings

class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]
    
class FTPTLSSession(FTP_TLS):

    def __init__(self, server, login, password):
        FTP_TLS.__init__(self)
        self.connect(server)
        self.login(login, password)
        self.prot_p()

class Ftp():
    __metaclass__ = Singleton
    
    ftp         = ''
    server      = settings['backup_server_ip']
    #server     = 'dev.sn8.com.br'
    login       = ''
    password    = ''
    
    def __init__(self, login, password):
        self.login      = login
        self.password   = password
        
        
    def connect(self):
        try:
            print "Conectando ao host %s com o usuario %s" %(self.server, self.login)
            self.ftp = ftputil.FTPHost(self.server, self.login, self.password)#, session_factory=FTPTLSSession)
            return self.ftp
        except Exception, e:
            print "nao foi possivel conectar. ", str(e)
            return False
    
    def disconnect(self):
        try:
            print "Desconectando o usuario $s do host %s " %(self.login, self.server)
            self.ftp.close()
        except Exception, e:
            print "nao foi possivel desconectar. ", str(e)
            return False
        
    def isClosed(self):
        if self.ftp.closed:
            return True
                
    def upload(self, source, target, login):
        try:
            print "Usuario %s realizando upload de %s para %s no host %s " %(login, source, target, self.server)
            self.ftp.upload(source, target)
            return True
        except Exception, e:
            print "upload do arquivo falhou.", str(e)
            self.disconnect()
            return False
        
    def list(self, path):
        directories = []
        files       = []
        
        try:
            self.ftp.chdir(path)
            
            list = self.ftp.listdir(self.ftp.curdir)

            for l in list:
                if not l.startswith("."):
                    if self.ftp.path.isdir(l):
                        directories.append(l)
                    elif self.ftp.path.isfile(l):
                        files.append(l)
            
            return directories, files, self.ftp.getcwd() 
            
        except Exception, e:
            print "A listagem falhou.", str(e)
            self.disconnect()
            return False
    
    def cd(self, path):
        try:
            self.ftp.chdir(path)
            return True
        except Exception, e:
            print "Erro ao acessar diretorio", str(e)
            return False
        
    def download(self, source, target):
        try:
            self.ftp.download(source, target)
            return True
        except Exception, e:
            print "Erro ao realizar download", str(e)
            return False
        
    
    def mkdir(self, path):
        try:
            self.ftp.mkdir(path)
            return True
        except Exception, e:
            print "Erro ao criar diretorio, ", str(e)
            return False
        
    def mkdirIfNotExists(self, path):
        try:
            self.ftp.mkdir(path)
            return True
        except Exception, e:
            #print "error ao criar diretorio", str(e)
            return True
        
    def rename(self, source, target):
        try:
            if self.ftp.path.isdir(source):
                self.ftp.rename(source, target)
                return True, 0
            else:
                return False, 1
        except Exception, e:
            print "Erro ao renomear, ", str(e)
            return False, 0
        
    def delete(self, path):
        try:
            if self.ftp.path.isdir(path):
                
                if not self.ftp.listdir(path):
                    self.ftp.rmdir(path)
                else:
                    return False, 1
            
            elif self.ftp.path.isfile(path):
                self.ftp.remove(path)
                
            return True, 0
        
        except Exception, e:
            print "Erro ao deletar, ", str(e)
            return False, 0
        
    def deleteDirAndFiles(self, path):
        try:
            self.ftp.rmtree(path, ignore_errors=True, onerror=None)
            return True
        except Exception, e:
            print "Erro ao deletar, ", str(e)
            return False
    
        
    def rmdir(self):
        pass
    
    def isEmpty(self):
       pass
       
    def isDir(self, path):
        if self.ftp.path.isdir(path):
            return True
        else:
            return False
        
    def current(self):
        self.ftp.chdir('testes/users/ricardo/')
        print self.ftp.getcwd() 
        
if __name__ == '__main__':
    f = Ftp('dev', 'guloseimas')
    f.connect()
    f.current()
    f.dowload('a.txt', './a.txt')
    #l = f.list('testes/users/ricardo/a/')
    
    #f.upload('/var/www/html/webservice/py/ftp/Request.py', 'Request.py', 'ricardo')