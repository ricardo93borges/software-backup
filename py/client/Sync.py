# -*- coding: UTF-8 -*-
from os import walk
from requests.auth import HTTPBasicAuth, HTTPDigestAuth
from Ftp import Ftp
from File import File
import sys, time, os
import requests
import bcrypt
import Request
import base64
import json
import ast
from time import sleep

import pdb

class Sync():
    
    ftp         = None
    usuario     = ''
    password    = ''
    schedules   = []
    localDirectories = []
    #path = 'testes/users/ricardo'
    path = ''    

    def __init__(self, user, password):
        
        self.usuario = user
        self.password = password
        self.path = '/home/'+base64.b64decode(user)
        
    def connect(self):
        if self.ftp == None or self.ftp.isClosed():
            self.ftp = Ftp(base64.b64decode(self.usuario), base64.b64decode(self.password))
            self.ftp.connect()
            
    def selectSchedules(self):
        try:
            url     = Request.urls['schedulesToBackup']
            user    = self.usuario
            passwd  = self.password
            
            request = requests.get(url, auth=HTTPBasicAuth(user, passwd))
             
            if(Request.handleResponse(request)):
                j = request.json()
                self.schedules = json.loads(j['schedules'])
                return True
            else:
                print "Nao foi possivel selecionar os agendamentos"
                return False
        
        except Exception, e:
            print "Erro ao selecionar agendamentos", str(e)
            
    def size_readable(self, num, suffix='B'):
        for unit in ['','K','M','G','T','P','E','Z']:
            if abs(num) < 1024.0:
                return "%3.1f%s%s" % (num, unit, suffix)
            num /= 1024.0
        return "%.1f%s%s" % (num, 'Yi', suffix)
          
    def setRemotePath(self, dirs, root):
        try:
            for d in dirs:
                
                if d == root:
                    dirs[d]['remotePath'] = self.path
                    continue
                
                path = dirs[d]['path']
                spt = path.split('/')
                for s in spt:
                    if s == root:
                        i = spt.index(s)
                        remotePath = '/'.join(spt[i:])
                        
                        dirs[d]['remotePath'] = self.path+'/'+remotePath
                        break
                    
            return dirs
        except Exception, e:
            print 'Erro ao definir diretorio remoto, ', str(e)
              
    def getContents(self):
        for s in self.schedules:
            try:
                path = s['directory']
                print path
                contents = os.listdir(path[0])
                
                tree = {}
                
                pathSplit = path.split('/')
                
                topDir = pathSplit[-1]
                p = '/'.join(pathSplit[:-1])
                
                tree[topDir] = {'path':p, 'remotePath':'', 'content':{}}

                localDirectory = {
                                  'Nome':topDir, 
                                  'Diretorio':path, 
                                  'directories':{},
                                  'schedule' : s['id']
                                  }

                for root, dirs, files in walk(path):
                    for d in dirs:
                        tree[d] = {'path':root, 'remotePath':'', 'content':{}}
    
                    for fileName in files:
                        source = root.split('/')[-1]
                        
                        file = {'source':source, 'type':'file'}
                        tree[source]['content'][fileName] = file
    
                tree2 = self.setRemotePath(tree, topDir)
                localDirectory['directories'] = tree2
                
                self.localDirectories.append(localDirectory)
                
            except Exception, e:
                print 'Erro ao selecionar diretorio, ', str(e)
    
    def setDirs(self, paths, root):
        dirs = []
        
        for p in paths:
            spt = p.split('/')
            for s in spt:
                if s == root:
                    i = spt.index(s)
                  
                    path = '/'.join(spt[i:])
                    dirs.append(path)
                    break
                    
        return sorted(dirs, key=len)
            
    def upload(self):
        try:
            self.connect()
            
            creedences = {'user':base64.b64decode(self.usuario), 'passwd':base64.b64decode(self.password)}
            
            for dir in self.localDirectories:
                paths = [dir['directories'][d]['path']+'/'+d for d in dir['directories']]
                dirsToCreate = self.setDirs(paths, dir['Nome'])
                
                for d in dirsToCreate:
                    self.ftp.mkdirIfNotExists(self.path+'/'+d)
                
                dir['Status'] = 'Enviando'
            
                #pdb.set_trace()
                for d in dir['directories']:
                    for c in dir['directories'][d]['content']:
                        
                        file        = dir['directories'][d]['content'][c]
                        directory   = dir['directories'][d]
                        
                        path     = directory['path']+'/'+file['source']+'/'+c
                        location = directory['remotePath']+'/'+file['source']
    
                        f = File(c, path, location, 3)
                        f.add(creedences, self.ftp)
            
            self.updateLastBackupDate(dir['schedule'])            
        except Exception, e:
            print "Erro no upload", str(e)
            
    def updateLastBackupDate(self, id):
        try:
            url     = Request.urls['updateScheduleLastBackup']
            user    = self.usuario
            passwd  = self.password
            
            schedule = ''
            
            for s in self.schedules:
                if s['id'] == str(id):
                    schedule = s
                    continue
            
            data = {'id':schedule['id']}
            
            request = requests.put(url, auth=HTTPBasicAuth(user, passwd), data=data)

            if(Request.handleResponse(request)):
                return True
            else:
                print "Nao foi possivel atualizar a data do ultimo backup"
                return False
        
        except Exception, e:
            print "Erro ao atualizar agendamento", str(e)
        
def getConfigs():
    try:
        f = open("config.txt", "r+")
        str = ast.literal_eval(f.read())
        f.close()
        if type(str) == dict:
            return str
        else:
            return False
    except Exception, e:
        print "Erro ao abrir arquivo de configuracao"
        
if __name__ == '__main__':
    config = getConfigs()
    
    while True:
        if config and config[0] and config[1]:
            s = Sync(config[0], config[1])
            s.selectSchedules()
        
            if s.schedules:
                s.connect()
                s.getContents()
                s.upload()

        sleep(3600)