from PySide.QtGui import *
from PySide.QtCore import *
import threading
import time

class CustomLabel(QLabel):
    
    sigDestroy  = Signal()
    sigUpdate   = Signal(str, int)

    def __init__(self):
        QLabel.__init__(self)
    
        self.sigDestroy.connect(self.destroyLabel)
        self.sigUpdate.connect(self.upadateLabel)
    
    def destroyLabel(self):
        self.hide()
    
    def upadateLabel(self, str, showFor):
        self.show()
        t = threading.Thread(target=self.setLabelText, args=([str, showFor]))
        t.start()
        
    def setLabelText(self, text, showFor):
        self.setText(text)
        
        if showFor:
            time.sleep(showFor)
            self.setText('')
    