import requests
from warnings import catch_warnings
from requests.models import Response
from settings import settings

domain = settings['webservice_domain']

urls = {
        'addUser'                   : domain+'/user',
        'auth'                      : domain+'/auth',
        
        'addFile'                   : domain+'/arquivo',
        'deleteFile'                : domain+'/arquivo',
        'updateFileStatus'          : domain+'/arquivo',
        'selectFiles'               : domain+'/arquivos',
        'selectFile'                : domain+'/arquivo',
        
        'addSchedule'               : domain+'/schedule',
        'selectSchedules'           : domain+'/schedules',
        'selectSchedule'            : domain+'/schedule',
        'updateSchedule'            : domain+'/schedule',
        'deleteSchedule'            : domain+'/schedule',
        'schedulesToBackup'         : domain+'/schedulesToBackup',
        'updateScheduleLastBackup'  : domain+'/updateScheduleLastBackup'
        }
  
def handleResponse(request):
    code = request.status_code

    if code == 200:
        try:
            response = request.json()
        except:
            print "Status: ", code, ", Nao foi possivel retornar um json da requisicao"
            return False
        
        if response['return']   == 'true':
            return True
        
        elif response['return'] == 'false':
            return False
            
    elif code == 401:
        print "Status: ", code, ", Acesso nao autorizado."
    elif code == 404:
        print "Status: ", code, ", Requisicao invalida."
    else:
        print "Status: ", code, ", Falha na requisicao"
        
    return False