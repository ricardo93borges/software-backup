import select
import socket
import sys
import signal
import cPickle
import struct
import re
import time
SERVER_HOST = 'localhost'
CHAT_SERVER_NAME = 'server'
    
class Server(object):
    """ An example chat server using select """
    
    def __init__(self, port=12355, backlog=5):
        print "init"
        self.clients    = 0
        self.clientmap  = {}
        self.outputs    = [] # list output sockets
        self.server     = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # Enable re-using socket address
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.bind((SERVER_HOST, port))
        print 'Server listening to port: %s ...' %port
        self.server.listen(backlog)
        # Catch keyboard interrupts
        signal.signal(signal.SIGINT, self.sighandler)
        
    def send(self, channel, msg):
        channel.send(msg)
   
    def receive(self, channel):
        buf = channel.recv(1024)
        return buf
    
    #modelo de string recebida (('127.0.0.1', 44371), "a")     
    def receiveTuple(self, channel):
        buf = channel.recv(1024)
        return buf
    
    def recv_basic(self, sock):
        total_data=[]
        while True:
            print "receiving .."
            data = sock.recv(1024)
            
            if not data: 
                print "breaking"
                break
            
            total_data.append(data)
            print len(total_data)
            
        return ''.join(total_data)
    
    def recvFile(self, sock):
        total_data=[]
        totalSize = 2169888
        
        while sizeRecv < totalSize:
            data = sock.recv(1024)
            total_data.append(data)
            sizeRecv = len(total_data)
            print "sizeRecv ", sizeRecv
            
        print "done"

    def getSockname(self, data):
        m = re.match('\((.*?)\)', data) 
        s = m.group()
        sockname = s[1:]
        return sockname
    
    def getMessage(self, data):
        regex = re.compile("\"(.*?)\"")
        r = regex.search(data)
        message = r.group()
        return message
    
    def sighandler(self, signum, frame):
        print 'Desligando socket server...'
        for output in self.outputs:
            output.close()
        self.server.close()
        
    def get_client_name(self, client):
        info = self.clientmap[client]
        host, name = info[0][0], info[1]
        return '@'.join((name, host))
    
    def getOutput(self, w):
        print "w", w.getsockname()
        ip   = w.getsockname()[0]
        port = str(w.getsockname()[1])
        uid = ip+'_'+port
        print uid
        print self.clientmap
        output = self.clientmap[uid]
        return output
        
    def run(self):
        print "running ..."
        inputs = [self.server, sys.stdin]
        self.outputs  = []
        self.clientes = {}
        running = True
    
        while running:
            try:
                readable, writeable, exceptional = select.select(inputs, self.outputs, [])
            except select.error, e:
                break
            
            for sock in readable:
                if sock == self.server:
                    #server socket
                    client, address = self.server.accept()
                    print "conexao de %d de %s" %(client.fileno(), address)

                    cname = self.receive(client).split('NAME: ')[1]
                    self.clients += 1
                    inputs.append(client)
                    self.clientmap[client] = (address, cname) 
                   # self.send(client, "voce esta conectado.")              
                    self.outputs.append(client)
                    self.clientes[str(address)] = client
                    
                elif sock == sys.stdin:
                    #entrada padrao
                    junk    = sys.stdin.readline()
                    running = False
                else:
                    #outros sockets
                    print "outros"
                    try:
                        #data  = self.receive(sock)
                        print "receiving.."
                        data = self.recvFile(sock)
                        #data = self.receiveTuple(sock)
                        if data:
                            print "data"
                            #message  = self.getMessage(data)
                            #sockname = self.getSockname(data)
                            #sock     = self.clientes[sockname]
                            #self.send(sock, "msg")
                        else:
                            print "Server: %d desligou " % sock.fileno()
                            self.clients -= 1
                            sock.close()
                            inputs.remove(sock)
                            self.outputs.remove(sock)
                            
                    except socket.error, e:
                        # Remove
                        inputs.remove(sock)
                        self.outputs.remove(sock)
                        
        self.server.close()