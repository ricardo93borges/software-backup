import select
import socket
import sys
import pdb
SERVER_HOST      = 'localhost'
CHAT_SERVER_NAME = 'server'

class Client(object):
    
    def __init__(self, name='client', port=12355, host=SERVER_HOST):
        print "init"
        self.name = name
        self.connected = False
        self.host = host
        self.port = port
        #self.prompt='[' + '@'.join((name, socket.gethostname().split('.')[0])) + ']> '
        
    def connect(self):
        print "conectando ..."
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.connect((self.host, self.port))
            print "Conectado ao server@ port %d" % self.port
            self.connected = True
            #Envia meu nome
            self.send(self.sock, 'NAME: '+self.name)
            #data = self.receive(self.sock)
            #print "data ", data
        except socket.error, e:
            print "Falha ao conectar ao server @ port %d" % self.port
            sys.exit(1)
            
    def send(self, channel, msg):
        channel.send(msg)
        
    def sendTuple(self, channel, msg):
        msg      = '"'+msg+'"'
        sockname = channel.getsockname()
        message  = str((sockname, msg))
        channel.send(message)
        
    def sendFiles(self, sock=''):
        sock = self.sock
        f    = "img.jpg"
        file = open(f, "rb").read()
        print "sending ", len(file)
        sock.sendall(file)
        
    def receive(self, channel):
        buf = channel.recv(1024)
        return buf
            
    def run(self):
        print "running ..."
        while self.connected:
            try:
                #sys.stdout.write(self.prompt)
                #sys.stdout.flush()
                # Wait for input from stdin and socket
                readable, writeable, exceptional = select.select([0,self.sock], [],[])
                for sock in readable:
                    data = ''
                    if sock == 0:
                        #self.sendFiles(self.sock)
                        self.sendFiles(self.sock)
                        #self.send(self.sock, "OK")
                        """data = sys.stdin.readline().strip()
                        if data: 
                            self.sendTuple(self.sock, data)
                            self.sendFiles(self.sock)
                        """    
                    elif sock == self.sock:
                        data = self.receive(self.sock)
                    if not data:
                        print 'Cliente desligando.'
                        self.connected = False
                        break
                    else:
                        print "os seguintes dados foram recebidos:"
                        sys.stdout.write(data + '\n')
                        self.send(self.sock, "OK2")
                        #sys.stdout.flush()
            except KeyboardInterrupt:
                print " Client interrupted. """
                self.sock.close()
                break