import requests
import bcrypt
import Request
import base64
from requests.auth import HTTPBasicAuth, HTTPDigestAuth


class User2():
    
    login       = ''
    password    = ''
    status      = 0
    x=0
    
    def __init__(self, login, password, status):
        self.login      = login
        self.password   = password
        self.status     = status
    
    def crypt(self, value):
        return bcrypt.hashpw(value, bcrypt.gensalt())
        
    def add(self, url, creedences = {}):
        user    = creedences['user']
        passwd  = creedences['passwd']
        
        cryptedPasswd = self.crypt(self.password)
        data = {'login': self.login, 'password': cryptedPasswd}
        
        request = requests.post(url, auth=HTTPBasicAuth(user, passwd), data=data)
        
        Request.handleResponse(request)
        
    def auth(self):
        url     = Request.urls['auth']
        user    = base64.b64encode(self.login)
        passwd  = base64.b64encode(self.password)
        data    = {}
        
        request  = requests.post(url, auth=HTTPBasicAuth(user, passwd), data=data)
        
        if(Request.handleResponse(request)):
            response = request.json()
            return response['return']
        else:
            return 'false'