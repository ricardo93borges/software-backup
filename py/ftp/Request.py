import requests
from warnings import catch_warnings
from requests.models import Response

urls = {
        'addUser'           : 'http://localhost/webservice/index.php/user',
        'addFile'           : 'http://localhost/webservice/index.php/arquivo',
        'deleteFile'        : 'http://localhost/webservice/index.php/arquivo',
        'updateFileStatus'  : 'http://localhost/webservice/index.php/arquivo',
        'selectFiles'       : 'http://localhost/webservice/index.php/arquivos',
        'selectFile'        : 'http://localhost/webservice/index.php/arquivo',
        'auth'              : 'http://localhost/webservice/index.php/auth'
        }
  
def handleResponse(request):
    code = request.status_code

    if code == 200:
        try:
            response = request.json()
        except:
            print "Status: ", code, ", Nao foi possivel retornar um json da requisicao"
            return False
        
        if response['return']   == 'true':
            return True
        
        elif response['return'] == 'false':
            return False
            
    elif code == 401:
        print "Status: ", code, ", Acesso nao autorizado."
    elif code == 404:
        print "Status: ", code, ", Requisicao invalida."
    else:
        print "Status: ", code, ", Falha na requisicao"
        
    return False