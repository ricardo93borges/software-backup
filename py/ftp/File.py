import requests
import bcrypt
import Request
import os
import base64
from requests.auth import HTTPBasicAuth, HTTPDigestAuth
from Ftp import Ftp
from blueman.main.Config import path

class File():
    
    ftp         = None
    name        = ''
    path        = ''        #diretorio local
    location    = 'users/'  #diretorio remoto
    user        = 0
    status      = 0
    
    def __init__(self, name='', path= '', location='', status=''):
        self.name = name
        self.path = path
        self.location = location
        #self.user = user
        self.status = status
        
    def crypt(self, value):
        return bcrypt.hashpw(value, bcrypt.gensalt())
        
    def add(self, creedences = {}, connection=None):
        url     = Request.urls['addFile']
        user    = base64.b64encode(creedences['user'])
        passwd  = base64.b64encode(creedences['passwd'])
        location = self.location
        
        data = {'name': self.name, 'location': location, 'status':self.status}
        
        request = requests.post(url, auth=HTTPBasicAuth(user, passwd), data=data)
        
        if(Request.handleResponse(request)):
            response = request.json()
        
            id          = response['info']['id']
            userLogin   = response['info']['user']['login']
            
            #if(self.upload(self.name, self.path, userLogin)):
            target = self.location+'/'+self.name

            if(self.upload(self.path, target, creedences, connection)):
                if(self.updateStatus(id, 4, creedences)):
                    return True
        else:
            response = request.json()
            print response['message']
        
    def updateStatus(self, id, status, creedences = {}):
        url     = Request.urls['updateFileStatus']
        user    = base64.b64encode(creedences['user'])
        passwd  = base64.b64encode(creedences['passwd'])
        data = {'id': id, 'status': status}
        
        request = requests.put(url, auth=HTTPBasicAuth(user, passwd), data=data)

        if(Request.handleResponse(request)):
            print "Arquivo adicionado"
            return True
        else:
            print "Arquivo nao adicionado"
            return False
        
    def selectFiles(self, creedences = {}):
        url     = Request.urls['selectFiles']
        user    = base64.b64encode(creedences['user'])
        passwd  = base64.b64encode(creedences['passwd'])
 
        request = requests.get(url, auth=HTTPBasicAuth(user, passwd))
        
        if(Request.handleResponse(request)):
            response = request.json()
            for f in response['info']['files']:
                print "id", f['id']
                print "name", f['name']
                print "location", f['location']
                print "status_id", f['status_id']
                print "-------"

    def upload(self, source, target, creedences, connection):
        ext = os.path.splitext(source)[1]

        self.ftp = connection
        
        if self.ftp == None or self.ftp.isClosed():
            self.ftp = Ftp(creedences['user'], creedences['passwd'])
            self.ftp.connect()
            
        self.ftp.upload(source, target, creedences['user'])
        return True
    
    def delete(self, creedences, deleteType):
        
        url     = Request.urls['deleteFile']
        user    = base64.b64encode(creedences['user'])
        passwd  = base64.b64encode(creedences['passwd'])
        data    = {'name': self.name, 'location':self.location, 'status': 5, 'deleteType':deleteType}
    
        request = requests.delete(url, auth=HTTPBasicAuth(user, passwd), data=data)

        if(Request.handleResponse(request)):
            return True
        else:
            return False
    
if __name__ == "__main__":
    f = File('clock.py', 'users/ricardo', 3)
    f.add({'user':'ricardo', 'passwd':'password'})