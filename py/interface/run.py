from MainWindow import MainWindow
from Widget import Widget
from LoginWidget import LoginWidget
from PySide.QtGui import *
import sys, time

class Run():
    
    def __init__(self):
        myApp = QApplication(sys.argv)
    
        mainWindow = MainWindow()
        widget = Widget()
        widget.progressBar = mainWindow.progressBar
        widget.statusLabel = mainWindow.statusLabel
        mainWindow.setCentralWidget(widget)
    
        lw = LoginWidget(mainWindow, widget)
        lw.show()
        
        myApp.exec_()
        sys.exit(0)

if __name__ == '__main__':
    r = Run()