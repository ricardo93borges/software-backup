import requests
import bcrypt
import Request
from requests.auth import HTTPBasicAuth, HTTPDigestAuth

class User():
    
    login       = ''
    password    = ''
    status      = 0
    
    def __init__(self, login, password, status):
        self.login      = login
        self.password   = password
        self.status     = status
    
    def crypt(self, value):
        return bcrypt.hashpw(value, bcrypt.gensalt())
        
    def add(self, url, creedences = {}):
        user    = creedences['user']
        passwd  = creedences['passwd']
        
        cryptedPasswd = self.crypt(self.password)
        data = {'login': self.login, 'password': cryptedPasswd}
        
        request = requests.post(url, auth=HTTPBasicAuth(user, passwd), data=data)
        
        Request.handleResponse(request)
 
        