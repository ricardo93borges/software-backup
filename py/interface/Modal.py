import sys, time, os
from PySide.QtGui import *
from PySide.QtCore import *

class Modal(QDialog):
    
    def __init__(self, title, message):
        
        QDialog.__init__(self)
        self.findLabel = QLabel(message)
        
        self.mainLayout = QHBoxLayout()        
        self.mainLayout.addWidget(self.findLabel)

        self.setWindowTitle(title)
        self.setLayout(self.mainLayout)
        self.exec_()

    def __del__(self):
        pass
    
    def close(self, *args, **kwargs):
        return QDialog.close(self, *args, **kwargs)
