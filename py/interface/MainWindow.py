import sys, time
from PySide.QtGui import *
from PySide.QtCore import *
from Widget import Widget
from CustomProgressBar import CustomProgressBar
from CustomLabel import CustomLabel

class MainWindow(QMainWindow, Widget):
    
    def __init__(self):
        QMainWindow.__init__(self)
        self.setWindowTitle("Backup")
        self.setGeometry(200, 250, 1200, 500)
        
        self.setWindowIcon(QIcon('img/icon.png'))
        
        self.createActions()
        self.createMenus()
        
        #self.fileMenu.addAction(self.selectFilesAction)
        #self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.sairAction)
        
        self.helpMenu.addAction(self.aboutAction)
        self.fileMenu.addSeparator()
        self.helpMenu.addAction(self.guideAction)
        
        self.createStatusBar()
    
    def about(self):
        QMessageBox.about(self, "Software de backup","Para obter instrucoes de como usar este software acesse o submenu Guia no menu Ajuda")
        
    def guide(self):
        QMessageBox.about(self, "Software de backup","Guia")
        
    def exit(self):
        self.close()
        
    def createMenus(self):
        self.fileMenu  = self.menuBar().addMenu('&Arquivo')
        self.helpMenu  = self.menuBar().addMenu('&Ajuda')
        
    def createActions(self):
        #self.selectFilesAction  = QAction(QIcon('img/folder.png'),  'Selecionar arquivos', self, shortcut="Ctrl+O", statusTip="Selecionar arquivos.", triggered=self.showDialog)
        self.aboutAction        = QAction(QIcon('img/about.png'),   'Sobre',    self,   shortcut="Ctrl+A",  statusTip="Sobre.", triggered=self.about)
        self.guideAction        = QAction(QIcon('img/help.png'),    'Guia',     self,   shortcut="Ctrl+G",  statusTip="Guia.",  triggered=self.guide)
        self.sairAction         = QAction(QIcon('img/close.png'),   'Sair',     self,   statusTip="Sair.",  triggered=self.exit)
        
    def createStatusBar(self):
        self.statusBar = QStatusBar()
        
        self.statusLabel = CustomLabel()
        self.progressBar = CustomProgressBar()
        self.progressBar.hide()
        
        self.statusBar.addWidget(self.statusLabel)
        self.statusBar.addWidget(self.progressBar)
        
        self.setStatusBar(self.statusBar)

if __name__ == '__main__':
    myApp = QApplication(sys.argv)
    mainWindow = MainWindow()
    mainWindow.show()
    myApp.exec_()
    sys.exit(0)