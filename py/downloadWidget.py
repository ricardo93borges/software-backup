import sys, time, os
from PySide.QtGui import *
from PySide.QtCore import *
from Modal import Modal
from hurry import filesize
from distutils.filelist import FileList
sys.path.append(os.path.abspath(os.path.dirname(__file__) + '/' + '../ftp'))
from File import File
from Ftp import Ftp
import threading
import time
import pdb

class Widget(QWidget):
    
    ftp      = None
    usuario  = ''
    password = ''
    files    = []
    path     = ''
    directories = [] 
    remoteFiles = []
    TableHeaders = ('Arquivo', 'Tamanho', 'Diretorio', 'Status')
    forbiddenDirectories = ['home', '/home', 'home/', '/home/', '/']
    progressBar = ''
    statusLabel = ''
        
    def __init__(self):
        QWidget.__init__(self)
        self.setWindowTitle("Main window")
        self.setGeometry(600, 250, 1000, 500)

        self.setupLayout()
        
    def connect(self):
        if self.ftp == None or self.ftp.isClosed():
            self.ftp = Ftp(self.usuario, self.password)
            self.ftp.connect()
        
    def setupLayout(self):
        layoutV = QVBoxLayout()
        layoutH = QHBoxLayout()
        
        #layout.addLayout(self.setupLoginForm())
        layoutH.addLayout(self.setupTable())
        layoutH.addLayout(self.setupList())
        #layout.addLayout(self.setupStatusBar())
        #layout.addLayout(self.setupActionButtons())
        
        layoutV.addLayout(layoutH)
        
        self.setLayout(layoutV)
        
    def setupLoginForm(self):
        layout = QHBoxLayout()
        formLayout = QFormLayout()
        
        labelUser = QLabel('Usuario', self)
        self.txtUser = QLineEdit(self)
        labelPassword = QLabel('Senha', self)
        self.txtPassword = QLineEdit(self)
        self.txtPassword.setEchoMode(QLineEdit.Password)
        
        btnLogin = QPushButton('Login',self)
        btnLogin.setMaximumWidth(150)
        btnLogin.clicked.connect(self.login)
        
        formLayout.addRow(labelUser,     self.txtUser)
        formLayout.addRow(labelPassword, self.txtPassword)
        formLayout.addWidget(btnLogin)
        
        layout.addLayout(formLayout)
        
        return layout
        
    def setupTable(self):
        layout = QVBoxLayout()
        
        self.table = QTableWidget(0, 4, self)
        self.table.setHorizontalHeaderLabels(['Arquivo', 'tamanho', 'diretorio', 'Status'])
        
        layout.addWidget(self.table)
        layout.addLayout(self.setupTableButtons())
        
        return layout
    
    def setupTableButtons(self):
        layout = QHBoxLayout()
        
        btnSelectFiles = QPushButton('Selecionar arquivos', self)
        btnSelectFiles.clicked.connect(self.showDialog)
        
        btnUpload = QPushButton('Enviar arquivos', self)
        btnUpload.clicked.connect(self.startUpload)
        
        btnClean = QPushButton('Limpar', self)
        btnClean.clicked.connect(self.cleanTable)
        
        layout.addWidget(btnSelectFiles)
        layout.addWidget(btnUpload)
        layout.addWidget(btnClean)
    
        return layout
    
    def setupList(self):
        layout = QVBoxLayout()
        
        self.txtPath = QLineEdit(self)
        self.txtPath.setReadOnly(True)
        
        self.list = QListWidget(self)
        
        self.list.doubleClicked.connect(self.accessDirectory)
        
        layout.addWidget(self.txtPath)
        layout.addWidget(self.list)
        layout.addLayout(self.setupListButtons())
        
        return layout

    def setupListButtons(self):
        layout = QHBoxLayout()
        
        btnBack = QPushButton('Voltar', self)
        btnBack.clicked.connect(self.back)
        
        btnDelete = QPushButton('Delete', self)
        btnDelete.clicked.connect(self.delete)
        
        btnDownload = QPushButton('Download', self)
        btnDownload.clicked.connect(self.download)
        
        btnCreateDir = QPushButton('Criar diretorio', self)
        btnCreateDir.clicked.connect(self.createDir)
        
        btnRename = QPushButton('Renomear', self)
        btnRename.clicked.connect(self.rename)
        
        layout.addWidget(btnBack)
        layout.addWidget(btnDelete)
        layout.addWidget(btnCreateDir)
        layout.addWidget(btnRename)
        layout.addWidget(btnDownload)
    
        return layout
    
    def back(self):
        list = self.path.split('/')
        newPath = ''
        
        list.pop()
        
        if list:
            newPath = '/'.join(list)
            
            if newPath and not newPath in self.forbiddenDirectories:
                self.path = newPath
                self.updateList(True)
            
    def delete(self):
        self.statusLabel.sigUpdate.emit('Deletando', 0)
        
        try:
            self.connect()
            
            i = self.list.currentItem()
            item = i.text()
            path = self.path+'/'+item
            
            if self.ftp.isDir(path):
                self.deleteDir(path)
            else:
                self.deleteFile(item, path)
            
        except Exception, e:
            print str(e)
            
    def deleteFile(self, item, path, deleteFromServer=True):
        
        if deleteFromServer:
            f = File(item, '', self.path, 5)
            creedences = {'user':self.usuario, 'passwd':self.password}
            
            if f.delete(creedences, 0):
                self.ftp.delete(path)
                self.updateList(True)
        else:
            f = File(item, '', path, 5)
            creedences = {'user':self.usuario, 'passwd':self.password}
            
            if f.delete(creedences, 0):
                return True
        
    def deleteDir(self, path):
        
        delete, ret = self.ftp.delete(path)
        
        if delete:
            self.updateList(True)
        elif ret == 1:
            msgBox = QMessageBox()
            msgBox.setText("Este diretorio nao esta vazio, deseja remover todos os arquivos nesse diretorio ?")
            msgBox.setInformativeText("O tempo deste processo depende da quantidade de arquivos e subdiretorios deste diretorios.")
            msgBox.setStandardButtons(QMessageBox.Cancel | QMessageBox.Ok)
            msgBox.setDefaultButton(QMessageBox.Cancel)
            r = msgBox.exec_()
            
            if r == QMessageBox.Ok:
                self.listDirsAndFiles(path)
        
    def listDirsAndFiles(self, path):        
        dirsList  = []
        filesList = []
      
        directories, files, path = self.ftp.list(path)
        
        itensLen = len(directories)+len(files)
        
        self.progressBar.sigInit.emit(itensLen)
        
        for d in directories:
            dict = {'name':d, 'path':path}
            dirsList.append(dict)

        for f in files:
            dic = {'name':f, 'path':path}
            filesList.append(dic)

        for file in filesList:
            self.deleteFile(file['name'], file['path'], False)
            self.progressBar.sigUpdate.emit()        
        
        self.progressBar.sigDestroy.emit()
        
        for d in dirsList:
            newPath = d['path']+'/'+d['name']
            self.listDirsAndFiles(newPath)

        if self.ftp.deleteDirAndFiles(path):
            self.updateList(True)
    
    def download(self):
        try:
            self.statusLabel.sigUpdate.emit('Fazendo download, aguarde', 0)
            self.connect()
            
            i = self.list.currentItem()
            item = i.text()
            source = self.path+'/'+item
            target = '../download'+item
            
            if self.ftp.download(source, target):
                self.statusLabel.sigUpdate.emit('Concluido', 5)
            else:
                self.statusLabel.sigUpdate.emit('O dowload falhou', 5)
        except Exception, e:
            print str(e)
            self.statusLabel.sigUpdate.emit('O dowload falhou', 5)
            
    
    def createDir(self):
        self.connect()
            
        text, ok = QInputDialog.getText(self, 'Nome do diretorio', 'Digite o nome do diretorio:')
        
        if ok and text and text != '.' and text != '..':
            self.ftp.mkdir(self.path+'/'+text)
            self.updateList(True)
    
    def rename(self):
        self.connect()
        
        i = self.list.currentItem()
        item = i.text()
        
        source = self.path+'/'+item
        
        text, ok = QInputDialog.getText(self, 'Novo nome', 'Digite o novo nome:')
        
        if ok and text and text != '.' and text != '..':
            target = self.path+'/'+text
            rename, ret = self.ftp.rename(source, target)
            
            if rename:
                self.updateList(True)
            elif ret == 1:
                QMessageBox.about(self, "Renomear","Nao e permitido renomear arquivos, apenas diretorios.")
        
    
    def login(self):
        usuario = self.txtUser.text()
        password = self.txtPassword.text()
    
    def startUpload(self):
        if self.files:
            filesToUpload = [d for d in self.files if d['Status']=='Pendente']

            t = threading.Thread(target=self.upload, name="thread-upload", args=([filesToUpload]))
            t.start()
            
    def upload(self, filesToUpload):        
        filesLen = len(filesToUpload)
        self.statusLabel.sigUpdate.emit('Enviando arquivos ', 0)
        self.progressBar.sigInit.emit(filesLen)
        
        self.connect()
        
        for file in filesToUpload:
            
            file['Status'] = 'Enviando'
            self.updateTableItem(file['Row'], 3, 'Enviando')
            
            f = File(file['Arquivo'], file['Diretorio'], self.path, 3)
            creedences = {'user':self.usuario, 'passwd':self.password}
            
            if f.add(creedences, self.ftp):
                file['Status'] = 'Enviado'
                self.updateTableItem(file['Row'], 3, 'Enviado')
                self.progressBar.sigUpdate.emit()
        
        self.statusLabel.sigUpdate.emit('Arquivos enviados ', 5)
        self.progressBar.sigDestroy.emit()        
        self.updateList(True)
                        
    def updateTableItem(self, row, column, item):
        self.table.setItem(row, column, QTableWidgetItem(item))
        
    def showDialog(self):
        dialog = QFileDialog(self)
        dialog.setDirectory('/var/www/html/webservice/py/interface')
        dialog.setFileMode(QFileDialog.ExistingFiles);
        
        itens = []
        
        if dialog.exec_():
            itens = dialog.selectedFiles();
        
        for f in itens:
            file = open(f, 'r')
            
            size = filesize.size(os.path.getsize(os.path.realpath(file.name)))
            name = file.name.split('/')[-1]
            path = os.path.realpath(file.name)
            
            d = {'Arquivo':str(name), 'Tamanho':str(size), 'Diretorio':str(path), 'Status':'Pendente'}
            self.files.append(d)
            
        self.updateTable(self.files)
        
    def updateTable(self, itens):
        self.table.clearContents()
        
        columnCount = self.table.columnCount()
        row         = 0
        column      = 0
        
        self.table.setRowCount(len(itens))
        
        for i in itens:
            while (column < columnCount):
                for t in self.TableHeaders:
                    self.table.setItem(row, column, QTableWidgetItem(i[t]))
                    i['Row'] = row
                    column += 1
            row += 1
            column = 0
            
    def cleanTable(self):
        try:
            self.table.clearContents()
            self.files = []
        except Exception, e:
            print "Erro ao limpar tabela", str(e)
            
    def updateList(self, clear=False):
        if clear:
            self.list.clear()
        
        self.connect()
            
        self.directories, self.remoteFiles, self.path = self.ftp.list(self.path)
        
        row = 0
        
        self.statusLabel.sigUpdate.emit('Atualizando lista ', 0)
        
        for i in self.directories:
           
            item = QListWidgetItem(QIcon('img/folder.png'), i)
            
            self.list.insertItem(row, item)
            row += 1
            
            #self.progressBar.sigUpdate.emit()
            
        for i in self.remoteFiles:
            item = QListWidgetItem(QIcon('img/file.png'), i)
            self.list.insertItem(row, item)
            row += 1
            
            #self.progressBar.sigUpdate.emit()
            
        try:
            list = self.path.split('/')
            list.remove('home')
            self.pathToShow = '/'.join(list)
            
            self.txtPath.setText(self.pathToShow)
        except Exception, e:
            pass
        
        self.statusLabel.sigUpdate.emit('Concluido ', 5)
    
    def accessDirectory(self):
        i = self.list.currentItem()
        dir = i.text()

        if dir in self.directories:
            self.path = self.path+'/'+i.text()
            self.updateList(True)
        
if __name__ == '__main__':
    myApp  = QApplication(sys.argv)
    widget = Widget()
    widget.show()
    myApp.exec_()
    sys.exit(0)