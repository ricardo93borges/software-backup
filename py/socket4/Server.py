import socket
import select
import os 
import sys
import errno
import random
import thread
import pdb
from time import sleep
from random import randint

class Server:
    host        = ""
    port        = ""
    inputs      = []
    outputs     = []
    server      = ""
    connection  = ""
    #actions     = {'sendMessage':self.sendMessage(), 'sendFile':self.sendFile()}
    actions     = ['sendMessage', 'sendFile']
    
    
    def __init__(self, port):
        self.host   = socket.gethostname()
        self.port   = port
        
        self.server = socket.socket()
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.setblocking(0)
        
        self.server.bind((self.host,self.port))
        self.server.listen(5)
        
        self.inputs = [self.server]
        self.outputs = []
        
        #return server
        
    def _select(self):
        while self.inputs:
            readable, writable, exceptional = select.select(self.inputs, self.outputs, self.inputs)
            
            for s in readable:
        
                if s is self.server:
                    # Um "readable" server socket esta pronto para aceitar uma conexao
                    self.connection, client_address = s.accept()
                    print >>sys.stderr, 'nova conexao de', client_address
                    self.connection.setblocking(True)
                    self.inputs.append(self.connection)
                    
                    self.connection.send('Conectado!')
                    
                    #thread.start_new_thread(self._select, ())
                    
                    self.whatToDo(self.connection, client_address)
                    
                    
            
                else:
                    #connection, client_address = s.accept()
                    # Interpreta resultado vazio como uma conexao fechada
                    print >>sys.stderr, 'fechando', client_address, 'depois de ler nenhum dado.'
                    # Para de ouvr por input na conexao
                    if s in self.outputs:
                        self.outputs.remove(s)
                    self.inputs.remove(s)
                    s.close()
            
            for s in writable:
                """try:
                    next_msg = message_queues[s].get_nowait()
                except Queue.Empty:
                    # No messages waiting so stop checking for writability.
                    print >>sys.stderr, 'output queue for', s.getpeername(), 'is empty'
                    outputs.remove(s)
                else:
                    print >>sys.stderr, 'sending "%s" to %s' % (next_msg, s.getpeername())
                    s.send(next_msg)
                """
            
            for s in exceptional:
                print >>sys.stderr, 'lidando com condicao excepcional para', s.getpeername()
                # Para de ouvir
                self.inputs.remove(s)
                if s in self.outputs:
                    self.outputs.remove(s)
                s.close()
                
    def whatToDo(self, connection, client_address):
        action = connection.recv(1024)
        print action
        
        if action in self.actions:
            if action == 'sendMessage':
                self.sendMessage(connection)
            elif action == 'sendFile':
                self.receiveFiles(connection)
        else:
            connection.send("Invalid action")
    
    def sendMessage(self, connection):
        connection.send("Mensagem")
        
    def sendFile(self):
        self.connection.send("Mensagem")
        
    def saveFile(self, name, ext, content):
        print "saving.."
        file    = open(name+ext, "w")
        file.write(content)
                
    def receiveFiles(self, connection):
        content = ""
        size    = 0
        
        fileSize = connection.recv(1024)
        fileName = connection.recv(1024)
        
        print "fileSize, fileName"
        print fileSize, fileName
        
        while(size < int(fileSize)):
            content += connection.recv(1024)
            file    = open(fileName, "w")
            file.write(content)
            file.close()
            size = os.path.getsize(fileName)
        
            print "recebido: "+str(size)+" / "+fileSize
            
        print "done"
 
    def validateFile(self):        
        print "validateFile"  
  