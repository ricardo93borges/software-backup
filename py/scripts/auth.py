import requests
import bcrypt
from requests.auth import HTTPBasicAuth, HTTPDigestAuth

user     = 'ricardo'
#password = bcrypt.hashpw('password', bcrypt.gensalt())
password = 'password';
passwd   = bcrypt.hashpw('password', bcrypt.gensalt())

url = 'http://localhost/webservice/index.php/auth'
print "url=> "+url

data = {'login': user, 'password': password}
r = requests.post(url, auth=HTTPBasicAuth('user', passwd), data=data)

print "r: ",        r
print "content: ",  r.content

#print r.json()