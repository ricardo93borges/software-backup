import requests
import bcrypt
from requests.auth import HTTPBasicAuth, HTTPDigestAuth
import base64

user     = 'user3'
password = bcrypt.hashpw('user3', bcrypt.gensalt())


url = 'http://177.220.193.148/webservice/index.php/user'
print "url=> "+url

data = {'login': user, 'password': password}
r = requests.post(url, auth=HTTPBasicAuth(base64.b64encode('ricardo'), base64.b64encode('password')), data=data)

print "r: ",        r
print "content: ",  r.content
#print "text: ",     r.text
print "password: ", password
#print r.json()

