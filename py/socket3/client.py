import socket     
import sys      
from sendfile import sendfile
import os

def enviar(sock, files):
    sock.send(str(len(files)))
    
    for filename in files:
        
        file = open(filename, "rb")
        f, ext = os.path.splitext(filename)
        blocksize = os.path.getsize(filename)
        offset = 0
        
        sock.send(ext)
        
        while True:
            sent = sendfile(sock.fileno(), file.fileno(), offset, blocksize)
            if sent == 0:
                print "enviado."
                break
            offset += sent

def conectar(socket, host, port):
    try:
        s.connect((host, port))
    except Exception, e:
        print "Erro ao conectar ( %s )" % (e)
        
        opt = raw_input("Tentar novamente? [s (Sim), n (Nao)]")
        if (opt == "s"):
            conectar(socket, host, port)
        else:
            print "Finalizando..."
            sys.exit(1)

s = socket.socket()        
host = socket.gethostname()
port = 12345               

files = ['file.txt', 'py_logo.png']
conectar(s, host, port)
enviar(s, files)

print s.recv(1024)

#s.close                    