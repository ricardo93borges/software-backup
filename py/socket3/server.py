import socket
import select
import sys
import errno
import random
from time import sleep
from random import randint

server = socket.socket()
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server.setblocking(0)

host = socket.gethostname()
port = 12345

server.bind((host,port))
server.listen(5)

inputs = [server]
outputs = []

def contarArquivos(connection):
    quantidade = connection.recv(1024)
    print quantidade
    
    count = 0
    while(count < int(quantidade)):
        print count
        dados = connection.recv(2048)
        print dados
        receberDados(connection, dados)
        count += 1
    
def receberDados(connection, dados):
    receberArquivo(connection, dados)
        
def receberArquivo(connection, dados):
    arquivo = connection.recv(2048)
    salvar(arquivo, dados)
   
def salvar(arquivo, dados):
    ext = dados
    #print ext
    #name = random.randint(1,10)+random.randint(1,20)+ext
    name = "arquivo"+ext
    novo = open(name, "w")
    novo.write(arquivo)
    #print file
    #print file.read()
    #connection.send('Arquivo lido.')
    
while inputs:
    readable, writable, exceptional = select.select(inputs, outputs, inputs)
    
    for s in readable:

        if s is server:
            # Um "readable" server socket esta pronto para aceitar uma conexao
            connection, client_address = s.accept()
            print >>sys.stderr, 'nova conexao de', client_address
            connection.setblocking(True)
            inputs.append(connection)
            
            connection.send('Voce esta conectado')
            #msg = connection.recv(1024)
            #file = connection.recv(2048)
            #ler(file)
            contarArquivos(connection)
            #c.close() 
    
            # Give the connection a queue for data we want to send
            #message_queues[connection] = Queue.Queue()
    
        else:
            # Interpreta resultado vazio como uma conexao fechada
            print >>sys.stderr, 'fechando', client_address, 'depois de ler nenhum dado.'
            # Para de ouvr por input na conexao
            if s in outputs:
                outputs.remove(s)
            inputs.remove(s)
            s.close()
    
    for s in writable:
        """try:
            next_msg = message_queues[s].get_nowait()
        except Queue.Empty:
            # No messages waiting so stop checking for writability.
            print >>sys.stderr, 'output queue for', s.getpeername(), 'is empty'
            outputs.remove(s)
        else:
            print >>sys.stderr, 'sending "%s" to %s' % (next_msg, s.getpeername())
            s.send(next_msg)
        """
    
    for s in exceptional:
        print >>sys.stderr, 'lidando com condicao excepcional para', s.getpeername()
        # Para de ouvit
        inputs.remove(s)
        if s in outputs:
            outputs.remove(s)
        s.close()
