import socket     
import sys      
from sendfile import sendfile
import os
import pdb
import threading

class Connection(threading.Thread):
    actions         = ['sendMessage', 'sendFile']
    connection      = ""
    clientAddress   = ""
    
    def __init__(self, connection, clientAddres):
        threading.Thread.__init__(self)
        self._stop = threading.Event()
        self.connection     = connection
        self.clientAddress  = clientAddres
        self.start()#chama o metodo run()
        self.handler()
    
    def stop(self):
        self._stop.set()

    def stopped(self):
        return self._stop.isSet()
        
    def run(self):
        msg = "Conectado! %s"%(self.getName())
        print 'nova conexao de', self.clientAddress
        self.connection.send(msg)
    
    def handler(self):
        action = self.connection.recv(1024)
        print action
        
        if action in self.actions:
            if action == 'sendMessage':
                self.sendMessage()
            elif action == 'sendFile':
                self.receiveFiles()
        else:
            self.connection.send("Invalid action")
            
    def sendMessage(self):
        self.connection.send("Mensagem")
        self.connection.close()

    def receiveFiles2(self):
        content = ""
        size    = 0
        
        fileSize = self.connection.recv(1024)
        fileName = self.connection.recv(1024)
        print "loop"
        while(size < int(fileSize)):
            content += self.connection.recv(4096)
        
            file    = open(fileName, "w")
            file.write(content)
            file.close()
            
            size = len(content)
        print "end"
        print "received ", size, "/", fileSize

        content = ""
        size    = 0
        self.connection.close()
        print "done"
                
    def receiveFiles(self):
        content = ""
        size    = 0
        
        fileSize = self.connection.recv(1024)
        fileName = self.connection.recv(1024)
                
        content += self.connection.recv(int(fileSize))
        file    = open(fileName, "w")
        file.write(content)
        file.close()

        self.connection.close()
        print "done"
        