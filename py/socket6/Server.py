import socket 
import pdb
from Connection import Connection

class Server:
    host        = ""
    port        = ""
    server      = ""
    connection  = ""
    
    def __init__(self, port):
        #self.host   = socket.gethostname()
        self.host   = '10.77.255.255'
        self.port   = port
        
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.setblocking(True)
        
        self.server.bind((self.host,self.port))
        self.server.listen(5)   
        
        print socket.gethostname()     
    
    def handler(self):
        while True:
            self.connection, client_address = self.server.accept()
            connection = Connection(self.connection, client_address)