import sys, time
from PySide.QtCore import *
from PySide.QtGui import *

class FileDialog(QMainWindow):
    
    def __init__(self):
       QMainWindow.__init__(self)
       
       self.textEdit = QTextEdit()
       self.setCentralWidget(self.textEdit)
       self.statusBar()
       
       openFile = QAction(QIcon('py_logo.png'), 'Open', self)
       openFile.setShortcut('Ctrl+o')
       openFile.setStatusTip('Open')
       openFile.triggered.connect(self.showDialog)
       
       menubar = self.menuBar()
       fileMenu = menubar.addMenu('&File')
       fileMenu.addAction(openFile)
       
       self.setGeometry(300,300,350,300) 
       self.setWindowTitle('File Dialog')
       self.show()
       
    def showDialog(self):
        fileName = QFileDialog.getOpenFileName(self, 'Open text files', '/var/www/html/webservice/py/pySide', 'Text files(*.txt *.py)')
        print fileName
        contents = open(fileName, 'r')  
            
        
        with contents:
            data = contents.read()
            self.textEdit.setText(data)
    
    
if __name__ == "__main__":
    app = QApplication(sys.argv)
    f   = FileDialog()
    f.show()
    app.exec_()
    sys.exit(0)
       
       