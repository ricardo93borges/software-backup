import sys, time
from PySide.QtGui import *

class MainWindow(QMainWindow):
    
    def __init__(self):
        QMainWindow.__init__(self)
        self.setWindowTitle("Main window")
        self.setGeometry(300, 250, 600, 500)
        self.statusLabel = QLabel('Showing progress')
        self.progressBar = QProgressBar()
        #self.progressBar.setMinimum(0)
        #self.progressBar.setMaximum(0)
        
    def setupComponents(self):
        self.textEdit = QTextEdit()
        self.setCentralWidget(self.textEdit)
        self.createActions()
        self.createMenus()
        self.createToolBar()
        
        self.fileMenu.addAction(self.newAction)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.exitAction)
        self.editMenu.addAction(self.copyAction)
        self.fileMenu.addSeparator()
        self.editMenu.addAction(self.pasteAction)
        self.helpMenu.addAction(self.aboutAction)
        
        self.mainToolBar.addAction(self.newAction)
        self.mainToolBar.addSeparator()
        self.mainToolBar.addAction(self.copyAction)
        self.mainToolBar.addAction(self.pasteAction)
        
    def newFile(self):
        self.textEdit.setText('')
        
    def exitFile(self):
        self.close()
        
    def aboutHelp(self):
        QMessageBox.about(self, "About simple text editor","asdf")
        
    def createActions(self):
        self.newAction      = QAction(QIcon('py_logo.png'), '&New',     self, shortcut=QKeySequence.New,    statusTip="Create a new file.",     triggered=self.newFile)
        self.exitAction     = QAction(QIcon('py_logo.png'), '&Exit',    self, shortcut="Ctrl+Q",            statusTip="Exit the application.",  triggered=self.exitFile)
        self.copyAction     = QAction(QIcon('py_logo.png'), '&Copy',    self, shortcut="Ctrl+C",            statusTip="Copy.",                  triggered=self.textEdit.copy)
        self.pasteAction    = QAction(QIcon('py_logo.png'), '&Paste',   self, shortcut="Ctrl+V",            statusTip="Paste.",                 triggered=self.textEdit.paste)
        self.aboutAction    = QAction(QIcon('py_logo.png'), '&About',   self, shortcut="Ctrl+A",            statusTip="About.",                 triggered=self.aboutHelp)
        
    def createMenus(self):
        self.fileMenu = self.menuBar().addMenu('&File')
        self.editMenu = self.menuBar().addMenu('&Edit')
        self.helpMenu = self.menuBar().addMenu('&Help')
        
    def createStatusBar(self):
        self.statusBar = QStatusBar()
        self.progressBar.setValue(10)
        self.statusBar.addWidget(self.statusLabel, 1)
        self.statusBar.addWidget(self.progressBar, 2)
        #self.statusBar.showMessage('Ready', 2000)
        self.setStatusBar(self.statusBar)
        
    def createToolBar(self):
        self.mainToolBar = self.addToolBar('Main')
        
    def showProgress(self):
        while(self.progressBar.value() < self.progressBar.maximum()):
            self.progressBar.setValue(self.progressBar.value() + 30)
            time.sleep(1)
        self.statusLabel.setText('Ready')
        
if __name__ == '__main__':
    myApp = QApplication(sys.argv)
    mainWindow = MainWindow()
    mainWindow.createStatusBar()
    mainWindow.setupComponents()
    mainWindow.show()
    mainWindow.showProgress()
    myApp.exec_()
    sys.exit(0)