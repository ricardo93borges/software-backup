import sys 
import time
from PySide.QtGui import QApplication, QWidget, QIcon, QLabel, QPushButton, QMessageBox, QDesktopWidget

class SampleWindow(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        self.setWindowTitle("Sample window")
        self.setGeometry(300, 300, 200, 150)
        self.setMinimumHeight(100)
        self.setMinimumWidth(250)
        self.setMaximumHeight(200)
        self.setMaximumWidth(800)
        self.center()
        
    def setAboutButton(self):
        self.aboutButton = QPushButton("About", self)
        self.aboutButton.move(100, 100)
        self.aboutButton.clicked.connect(self.showAbout)
        
    def showAbout(self):
        QMessageBox.about(self.aboutButton, "About Pyside", "bla bla blabla bla blabla bla blabla bla blabla bla blabla bla bla")
        
    def center(self):
        qRect = self.frameGeometry()
        centerPoint = QDesktopWidget().availableGeometry().center()
        qRect.moveCenter(centerPoint)
        self.move(qRect.topLeft())
        
    def quitApp(self):
        userInfo = QMessageBox.question(self, 'Confirmation', 'Do you want to continue?', QMessageBox.Yes | QMessageBox.No)
        if userInfo == QMessageBox.Yes:
            myApp.quit()
        if userInfo == QMessageBox.No:
            pass
    
    def setButton(self):
        myButton = QPushButton('quit', self)
        myButton.move(10, 100)
        #myButton.clicked.connect(myApp.quit)
        myButton.clicked.connect(self.quitApp)
        
    def setIcon(self):
        appIcon = QIcon('py_logo.png')
        self.setWindowIcon(appIcon)
        
    def setIconModes(self):
        myIcon1     = QIcon('Py_logo.png')
        myLabel1    = QLabel('sample', self)
        pixmap1     = myIcon1.pixmap(50, 50, QIcon.Active, QIcon.On)
        myLabel1.setPixmap(pixmap1)
        
        myIcon2     = QIcon('Py_logo.png')
        myLabel2    = QLabel('sample', self)
        pixmap2     = myIcon2.pixmap(50, 50, QIcon.Disabled, QIcon.Off)
        myLabel2.setPixmap(pixmap2)
        myLabel2.move(50, 0)
        
        myIcon3     = QIcon('Py_logo.png')
        myLabel3    = QLabel('sample', self)
        pixmap3     = myIcon3.pixmap(50, 50, QIcon.Selected, QIcon.On)
        myLabel3.setPixmap(pixmap3)
        myLabel3.move(100, 0)
        
        
if __name__ == '__main__':
    myApp    = QApplication(sys.argv)
    myWindow = SampleWindow()
    myWindow.setIcon()
    myWindow.setIconModes()
    myWindow.setButton()
    myWindow.setAboutButton()
    myWindow.show()
    #time.sleep(3)
    #myWindow.resize(500,300)
    #myWindow.setWindowTitle("Sample window resized")
    #myWindow.repaint()
    myApp.exec_()
    sys.exit(0)

        