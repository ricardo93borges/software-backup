import sys, time
from PySide.QtCore import *
from PySide.QtGui import *

class DragDrop(QWidget):
    
   def __init__(self):
       QWidget.__init__(self)
       self.list1 = QListWidget()
       self.list2 = QListWidget()
       
      # self.list2.setViewMode(QlistWidget.IconMode)
       
       self.list1.setAcceptDrops(True)
       self.list1.setDragEnabled(True)
       
       self.list2.setAcceptDrops(True)
       self.list2.setDragEnabled(True)
       
       self.setGeometry(300, 350, 500, 150)
       
       self.layout = QHBoxLayout()
       self.layout.addWidget(self.list1)
       self.layout.addWidget(self.list2)
       
       l1 = QListWidgetItem(QIcon('py_logo.png'), "Blue")
       l2 = QListWidgetItem(QIcon('py_logo.png'), "Red")
       
       self.list1.insertItem(1, l1)
       self.list2.insertItem(1, l2)
       
       self.setWindowTitle("Drag n Drop");
       self.setLayout(self.layout)
       
    
if __name__ == "__main__":
    app = QApplication(sys.argv)
    d = DragDrop()
    d.show()
    app.exec_()
    sys.exit(0)
       
       