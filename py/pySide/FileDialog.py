import sys, time
from PySide.QtCore import *
from PySide.QtGui import *

class FileDialog(QFileDialog):
    
   def __init__(self):
       QFileDialog.__init__(self)
       self.setFileMode(QFileDialog.AnyFile)
    
if __name__ == "__main__":
    app = QApplication(sys.argv)
    f   = FileDialog()
    f.show()
    app.exec_()
    sys.exit(0)
       
       