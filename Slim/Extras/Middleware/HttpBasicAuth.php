<?php
/**
 * HTTP Basic Authentication
 *
 * Use this middleware with your Slim Framework application
 * to require HTTP basic auth for all routes.
 *
 * @author Josh Lockhart <info@slimframework.com>
 * @version 1.0
 * @copyright 2012 Josh Lockhart
 *
 * USAGE
 *
 * $app = new \Slim\Slim();
 * $app->add(new \Slim\Extras\Middleware\HttpBasicAuth('theUsername', 'thePassword'));
 *
 * MIT LICENSE
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
//namespace Slim\Extras\Middleware;

require 'password.php';
require 'lib/database.php';

\Slim\Slim::registerAutoloader();
//use \Bcrypt;

class HttpBasicAuth extends \Slim\Middleware
{
    /**
     * @var string
     */
    protected $realm;

    /**
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $password;

    /**
     * Constructor
     *
     * @param   string  $username   The HTTP Authentication username
     * @param   string  $password   The HTTP Authentication password
     * @param   string  $realm      The HTTP Authentication realm
     */
  /*   public function __construct($username='', $password='', $realm = 'Protected Area'){
        $this->username = $username;
        $this->password = $password;
        $this->realm = $realm;
    } */
    
    public function __construct(){
    
    }
    
    function auth($login, $password){
    	try {
    		$c = getConnection();
    		$stat = $c->prepare('SELECT * FROM users WHERE login = :login');
    		$stat->bindParam(':login', $login);
    
    		$stat->execute();
    		$user = $stat->fetchAll();
    		$c = null;
    
    		if(count($user) == 1){
    			if(password_verify($password, $user[0]['password'])){
    				return true;
    			}
    		}
    		return false;
    	} catch (Exception $e) {
    		return json_encode($e->message());
    	}
    }
    
    public function call(){
    	$req = $this->app->request();
    	$res = $this->app->response();
    	$authUser = base64_decode($req->headers('PHP_AUTH_USER'));
    	$authPass = base64_decode($req->headers('PHP_AUTH_PW'));
    	if ($authUser && $authPass) {
    		
    		if($this->auth($authUser, $authPass)){
    			$this->next->call();
    		}else{
    			$res->status(401);
    			$res->header('WWW-Authenticate', sprintf('Basic realm="%s"', $this->realm));
    		}
    	} else {
    		$res->status(401);
    		$res->header('WWW-Authenticate', sprintf('Basic realm="%s"', $this->realm));
    	}
    }

    /**
     * Call
     *
     * This method will check the HTTP request headers for previous authentication. If
     * the request has already authenticated, the next middleware is called. Otherwise,
     * a 401 Authentication Required response is returned to the client.
     */
    //password_verify($password, $hash)
	 public function call2(){
		$req = $this->app->request();
		$res = $this->app->response();
		$authUser = $req->headers('PHP_AUTH_USER');
		$authPass = $req->headers('PHP_AUTH_PW');
		
		if ($authUser && $authPass && $authUser === $this->username && $authPass === $this->password) {
			$this->next->call();
		} else {
			$res->status(401);
			$res->header('WWW-Authenticate', sprintf('Basic realm="%s"', $this->realm));
		}
	}
    public function call3(){
    	$req = $this->app->request();
    	$res = $this->app->response();
    	$authUser = $req->headers('PHP_AUTH_USER');
    	$authPass = $req->headers('PHP_AUTH_PW');
    	if ($authUser && $authPass && $authUser === $this->username) {
    		if(password_verify($this->password, $authPass)){
    			$this->next->call();
    		}else{
    			$res->status(401);
    			$res->header('WWW-Authenticate', sprintf('Basic realm="%s"', $this->realm));
    		}
    	} else {
    		$res->status(401);
    		$res->header('WWW-Authenticate', sprintf('Basic realm="%s"', $this->realm));
    	}
    }   
}