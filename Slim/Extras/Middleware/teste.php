<?php
require 'password.php';

//namespace Slim\Extras\Middleware;

class HttpBasicAuth extends \Slim\Middleware{
    /**
     * @var string
     */
    protected $realm;

    /**
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $password;

    /**
     * Constructor
     *
     * @param   string  $username   The HTTP Authentication username
     * @param   string  $password   The HTTP Authentication password
     * @param   string  $realm      The HTTP Authentication realm
     */
    public function __construct($username, $password, $realm = 'Protected Area')
    {
        $this->username = $username;
        $this->password = $password;
        $this->realm = $realm;
    }

    public function call(){
    	$req = $this->app->request();
    	$res = $this->app->response();
    	$authUser = $req->headers('PHP_AUTH_USER');
    	$authPass = $req->headers('PHP_AUTH_PW');
    	if ($authUser && $authPass && $authUser === $this->username) {
    		if(password_verify($this->password, $authPass)){
    			$this->next->call();
    		}
    	} else {
    		$res->status(401);
    		$res->header('WWW-Authenticate', sprintf('Basic realm="%s"', $this->realm));
    	}
    }
}

$h = new HttpBasicAuth('user', 'password');
$h->call();
