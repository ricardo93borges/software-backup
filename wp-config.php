<?php

//Configuracoes de Producao
define('WP_DEBUG', false);

// ** FTP settings (CDN1) ** //
define('FTP_CDN_HOST', '10.16.210.130');
define('FTP_CDN_PORT', '21'); 
//define('FTP_CDN_URL', 'http://cdn1.mundodastribos.com/');
define('FTP_CDN_USER', 'cdn1');
define('FTP_CDN_PASWD', 'cdn1#100');
define('FTP_USED', true);

//define('CDN_URL', 'http://cdn.mundodastribos.com/');
//define('WP_SITEURL_SITEMAP', 'http://www.mundodastribos.com');
define('WP_BLOG_PUBLIC', '0');

if (strtolower($_SERVER['HTTP_HOST']) == "localhost"){
	define('WP_SITEPATH', '/var/www/html/mt/trunk');
	define('WP_SITEURL', 'http://localhost/mt/trunk');
	define('WP_HOME', 'http://localhost/mt/trunk');
	define('WP_CONTENT_DIR', '/var/www/html/mt/trunk/wp-content' );
	define('WP_CONTENT_URL', 'http://localhost/mt/trunk/wp-content');
	define('W_HOST_ADS','localhost');  //para imprimir title e meta tags e adsense
	define('WP_HOST_CSS_JS','http://localhost/mt/trunk'); //http://cdn1.mundodastribos.com
	
	define('DB_NAME', 'mt');
	define('DB_USER', 'root');     // Your MySQL username
	define('DB_PASSWORD', 'root'); // ...and password
	define('DB_HOST', 'localhost');    // 99% chance you won't need to change this value
	define('DB_CHARSET', 'utf8');
	define('DB_COLLATE', '');
}else if(strtolower($_SERVER['HTTP_HOST']) == "localhost"){
	// ** SITE URL ** //
	define('WP_SITEPATH', '/Users/ivml/workspace/trunk/');
	define('WP_SITEURL', 'http://localhost');
	define('WP_HOME', 'http://localhost');
	define('W_HOST_ADS','localhost');  //para imprimir title e meta tags e adsense
	define('WP_HOST_CSS_JS','http://localhost'); //http://cdn1.mundodastribos.com
	// ** MySQL settings ** //
	define('DB_NAME', 'mundodastribos');    // The name of the database
	define('DB_USER', 'root');     // Your MySQL username
	define('DB_PASSWORD', '$70dNovo#'); // ...and password
	define('DB_HOST', 'localhost');    // 99% chance you won't need to change this value
	define('DB_CHARSET', 'utf8');
	define('DB_COLLATE', '');
	define('WP_CACHE', true); //Added by WP-Cache Manager
	//adcionado por ivml para nao colocar nas urls ?doing_cron,chamado via crontab
	define('DISABLE_WP_CRON', true);
	//fim ivml
	define('ALTERNATE_WP_CRON', true);	
}else if (strtolower($_SERVER['HTTP_HOST']) == "homolog.mundodastribos.com"){
	// ** SITE URL ** //
	define('WP_SITEPATH', '/home/mundodastribos/www/');
	define('WP_SITEURL', 'http://homolog.mundodastribos.com');
	define('WP_HOME', 'http://homolog.mundodastribos.com');
	define('W_HOST_ADS','homolog.mundodastribos.com');  //para imprimir title e meta tags e adsense
	define('WP_HOST_CSS_JS','http://homolog.mundodastribos.com'); //http://cdn1.mundodastribos.com
	// ** MySQL settings ** //
	define('DB_NAME', 'mundodastribos');    // The name of the database
	define('DB_USER', 'root');     // Your MySQL username
	define('DB_PASSWORD', 'SSS3zdw9'); // ...and password
	define('DB_HOST', 'dba1');    // 99% chance you won't need to change this value
	define('DB_CHARSET', 'utf8');
	define('DB_COLLATE', '');
	define('WP_CACHE', true); //Added by WP-Cache Manager
	//adcionado por ivml para nao colocar nas urls ?doing_cron,chamado via crontab
	define('DISABLE_WP_CRON', true);
	//fim ivml
	define('ALTERNATE_WP_CRON', true);		
}else{
	 session_save_path('/home/mundodastribos/ibm_sessions');
	define('WP_SITEPATH', '/home/mundodastribos/www/');
	// ** MySQL settings ** //
	define('DB_NAME', 'mundodastribos');    // The name of the database
	define('DB_USER', 'mundodastribos');     // Your MySQL username
	define('DB_PASSWORD', '!bkf@fbr!'); // ...and password
	define('DB_HOST', 'dba3');    // 99% chance you won't need to change this value
	define('DB_CHARSET', 'utf8');
	define('DB_COLLATE', '');
	define('WP_CACHE', true); //Added by WP-Cache Manager
	define('ALTERNATE_WP_CRON', true);
	
	// ** SITE URL ** //
	define('WP_BLOG_PUBLIC', '1');
	define('WP_SITEURL', 'http://www.mundodastribos.com');
	define('WP_HOME', 'http://www.mundodastribos.com');
	define('W_HOST_ADS','www.mundodastribos.com');
	define('WP_HOST_CSS_JS','http://cdn.mundodastribos.com');
	
}



// You can have multiple installations in one database if you give each a unique prefix
$table_prefix  = 'wp_';   // Only numbers, letters, and underscores please!

// Change this to localize WordPress.  A corresponding MO file for the
// chosen language must be installed to wp-content/languages.
// For example, install de.mo to wp-content/languages and set WPLANG to 'de'
// to enable German language support.
define ('WPLANG', 'pt_BR');
define('WP_MEMORY_LIMIT', '32M');

//ivml - desabilita as revisoes de post e coloca o intervalo de rascunho automatico em 1 semana
define('WP_POST_REVISIONS', false );
define('AUTOSAVE_INTERVAL', 36288000 );
//fim ivml

//evitar quebrar js concatenados load-scripts.php
define('CONCATENATE_SCRIPTS', true);

/* That's all, stop editing! Happy blogging. */

define('ABSPATH', dirname(__FILE__).'/');
require_once(ABSPATH.'wp-settings.php');

 
?>
